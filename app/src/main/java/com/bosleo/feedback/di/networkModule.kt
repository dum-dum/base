package com.bosleo.feedback.di

import android.app.Application
import com.bosleo.feedback.api.TokenInterceptor
import com.bosleo.feedback.api.setCookieStore
import com.bosleo.feedback.data.candidate.datasource.remote.CandidateApiService
import com.bosleo.feedback.data.core.DateTimeTypeConverter
import com.bosleo.feedback.data.core.TimeConverter
import com.bosleo.feedback.data.department.datasource.remote.DepartmentApiService
import com.bosleo.feedback.data.form.datasource.remote.FormApiService
import com.bosleo.feedback.data.interview.datasource.remote.InterviewApiService
import com.bosleo.feedback.data.login.datasource.remote.LoginApiService
import com.bosleo.feedback.data.marks.datasorce.remote.InterviewMarksApiService
import com.bosleo.feedback.data.skill.datasource.remote.SkillApiService
import com.bosleo.feedback.data.subskill.datasource.remote.SubSkillApiService
import com.google.gson.GsonBuilder
import com.orhanobut.logger.Logger
import okhttp3.Cache
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.joda.time.DateTime
import org.joda.time.LocalTime
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { TokenInterceptor(get()) }

    single { createOkHttpClient(get(), get()) }

    single { createRetrofit(get(), "http://192.168.2.135:8080/", get()) }

    single { createWebService<CandidateApiService>(get()) }

    single { createWebService<LoginApiService>(get()) }

    single { createWebService<InterviewApiService>(get()) }

    single { createWebService<DepartmentApiService>(get()) }

    single { createWebService<FormApiService>(get()) }

    single { createWebService<SkillApiService>(get()) }

    single { createWebService<SubSkillApiService>(get()) }

    single { createWebService<InterviewMarksApiService>(get()) }


    single {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.registerTypeAdapter(DateTime::class.java, DateTimeTypeConverter())
        gsonBuilder.registerTypeAdapter(LocalTime::class.java, TimeConverter())
        return@single GsonConverterFactory.create(gsonBuilder.create())
    }

}

fun createOkHttpClient(application: Application, tokenInterceptor: TokenInterceptor): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
        Logger.d(it)
    })
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .retryOnConnectionFailure(true)
        .readTimeout(60L, TimeUnit.SECONDS)
        .setCookieStore(application)
        .connectionPool(ConnectionPool(0, 1, TimeUnit.NANOSECONDS))
        .addInterceptor(tokenInterceptor)
        .addInterceptor(httpLoggingInterceptor)
        .cache(Cache(createTempDir(), 10 * 1024 * 1024))
        .build()
}

fun createRetrofit(okHttpClient: OkHttpClient, url: String, gsonConverterFactory: GsonConverterFactory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(gsonConverterFactory)
//        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

inline fun <reified T> createWebService(retrofit: Retrofit): T {
    return retrofit.create(T::class.java)
}
package com.bosleo.feedback.di

import com.bosleo.feedback.data.candidate.contract.CandidateDataRepo
import com.bosleo.feedback.data.candidate.contract.CandidateRepo
import com.bosleo.feedback.data.department.contract.DepartmentDataRepo
import com.bosleo.feedback.data.department.contract.DepartmentRepo
import com.bosleo.feedback.data.form.contract.FormDataRepo
import com.bosleo.feedback.data.form.contract.FormRepo
import com.bosleo.feedback.data.interview.contract.InterviewDataRepo
import com.bosleo.feedback.data.interview.contract.InterviewRepo
import com.bosleo.feedback.data.login.contract.LoginDataRepo
import com.bosleo.feedback.data.login.contract.LoginRepo
import com.bosleo.feedback.data.marks.contract.InterviewMarksDataRepo
import com.bosleo.feedback.data.marks.contract.InterviewMarksRepo
import com.bosleo.feedback.data.skill.contract.SkillDataRepo
import com.bosleo.feedback.data.skill.contract.SkillRepo
import com.bosleo.feedback.data.subskill.contract.SubSkillDataRepo
import com.bosleo.feedback.data.subskill.contract.SubSkillRepo
import com.bosleo.feedback.data.syncmaster.MasterDataRepo
import com.bosleo.feedback.data.syncmaster.MasterRepo
import org.koin.dsl.module.module

val dataRepoModule = module {

    single<CandidateRepo> { CandidateDataRepo(get(), get(), get()) }

    single<LoginRepo> { LoginDataRepo(get(), get()) }

    single<InterviewRepo> { InterviewDataRepo(get(), get(), get()) }

    single<DepartmentRepo> { DepartmentDataRepo(get(), get()) }

    single<SkillRepo> { SkillDataRepo(get(), get()) }

    single<SubSkillRepo> { SubSkillDataRepo(get(), get()) }

    single<MasterRepo> { MasterDataRepo(get(), get(), get(), get(), get(), get(), get()) }

    single<FormRepo> { FormDataRepo(get(), get(), get(), get()) }

    single<InterviewMarksRepo> { InterviewMarksDataRepo(get(), get()) }
}
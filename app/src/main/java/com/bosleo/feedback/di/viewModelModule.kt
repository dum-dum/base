package com.bosleo.feedback.di

import com.bosleo.feedback.presentation.candidate.AddCandidateViewModel
import com.bosleo.feedback.presentation.candidate.CandidateViewModel
import com.bosleo.feedback.presentation.form.FormDetailViewModel
import com.bosleo.feedback.presentation.form.FormViewModel
import com.bosleo.feedback.presentation.home.HomeViewModel
import com.bosleo.feedback.presentation.interview.FeedbackViewModel
import com.bosleo.feedback.presentation.interview.InterviewViewModel
import com.bosleo.feedback.presentation.interview.RemarksViewModel
import com.bosleo.feedback.presentation.login.LoginViewModel
import com.bosleo.feedback.presentation.sample.SampleViewModel
import com.bosleo.feedback.presentation.splash.SplashViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val viewModelModule = module {

    viewModel { SampleViewModel(get()) }

    viewModel { LoginViewModel() }

    viewModel { HomeViewModel() }

    viewModel { SplashViewModel(get()) }

    viewModel { CandidateViewModel() }

    viewModel { InterviewViewModel() }

    viewModel { AddCandidateViewModel() }

    viewModel { FormViewModel() }

    viewModel { FormDetailViewModel() }

    viewModel { FeedbackViewModel() }

    viewModel { RemarksViewModel() }

}

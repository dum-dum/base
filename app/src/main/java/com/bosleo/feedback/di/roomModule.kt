package com.bosleo.feedback.di

import android.app.Application
import androidx.room.Room
import com.bosleo.feedback.data.candidate.datasource.local.CandidateDao
import com.bosleo.feedback.data.core.database.AppDataBase
import com.bosleo.feedback.data.department.datasource.local.DepartmentDao
import com.bosleo.feedback.data.form.datasource.local.FormDao
import com.bosleo.feedback.data.interview.datasource.local.InterviewDao
import com.bosleo.feedback.data.login.datasource.local.UserDao
import com.bosleo.feedback.data.marks.datasorce.local.InterviewMarksDao
import com.bosleo.feedback.data.skill.datasource.local.SkillDao
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillDao
import org.koin.dsl.module.module


val roomModule = module {

    single { roomDataBase(get()) }

    single { createCandidateDao(get()) }

    single { createUserDao(get()) }

    single { createInterviewDao(get()) }

    single { createDepartmentDao(get()) }

    single { createFormDao(get()) }

    single { createSkillDao(get()) }

    single { createSubSkillDao(get()) }

    single { createInterviewMark(get()) }

}

fun createUserDao(appDataBase: AppDataBase): UserDao {
    return appDataBase.userDao()
}

fun createCandidateDao(appDataBase: AppDataBase): CandidateDao {
    return appDataBase.candidateDao()
}

fun createInterviewDao(appDataBase: AppDataBase): InterviewDao {
    return appDataBase.interviewDao()
}

fun createDepartmentDao(appDataBase: AppDataBase): DepartmentDao {
    return appDataBase.departmentDao()
}

fun createFormDao(appDataBase: AppDataBase): FormDao {
    return appDataBase.formDao()
}

fun createSkillDao(appDataBase: AppDataBase): SkillDao {
    return appDataBase.skillDao()
}

fun createSubSkillDao(appDataBase: AppDataBase): SubSkillDao {
    return appDataBase.subSkillDao()
}

fun createInterviewMark(appDataBase: AppDataBase): InterviewMarksDao{
    return appDataBase.interviewMarkDao()
}


fun roomDataBase(context: Application): AppDataBase {
    return Room.databaseBuilder(context, AppDataBase::class.java, "sample.db")
        .fallbackToDestructiveMigration()
        .build()
}



package com.bosleo.feedback.di

import com.bosleo.feedback.data.candidate.datasource.local.CandidateLocalDS
import com.bosleo.feedback.data.candidate.datasource.local.CandidateLocalSource
import com.bosleo.feedback.data.candidate.datasource.remote.CandidateRemoteDS
import com.bosleo.feedback.data.candidate.datasource.remote.CandidateRemoteSource
import com.bosleo.feedback.data.department.datasource.local.DepartmentLocalDS
import com.bosleo.feedback.data.department.datasource.local.DepartmentLocalSource
import com.bosleo.feedback.data.department.datasource.remote.DepartmentRemoteDS
import com.bosleo.feedback.data.department.datasource.remote.DepartmentRemoteSource
import com.bosleo.feedback.data.form.datasource.local.FormLocalDS
import com.bosleo.feedback.data.form.datasource.local.FormLocalSource
import com.bosleo.feedback.data.form.datasource.remote.FormRemoteDS
import com.bosleo.feedback.data.form.datasource.remote.FormRemoteSource
import com.bosleo.feedback.data.interview.datasource.local.InterviewLocalDS
import com.bosleo.feedback.data.interview.datasource.local.InterviewLocalSource
import com.bosleo.feedback.data.interview.datasource.remote.InterviewRemoteDS
import com.bosleo.feedback.data.interview.datasource.remote.InterviewRemoteSource
import com.bosleo.feedback.data.login.datasource.local.LoginLocalDS
import com.bosleo.feedback.data.login.datasource.local.LoginLocalSource
import com.bosleo.feedback.data.login.datasource.remote.LoginRemoteDS
import com.bosleo.feedback.data.login.datasource.remote.LoginRemoteSource
import com.bosleo.feedback.data.marks.datasorce.local.InterviewMarkLocalDS
import com.bosleo.feedback.data.marks.datasorce.local.InterviewMarkLocalSource
import com.bosleo.feedback.data.marks.datasorce.remote.InterviewMarkRemoteDS
import com.bosleo.feedback.data.marks.datasorce.remote.InterviewMarkRemoteSource
import com.bosleo.feedback.data.skill.datasource.local.SkillLocalDS
import com.bosleo.feedback.data.skill.datasource.local.SkillLocalSource
import com.bosleo.feedback.data.skill.datasource.remote.SkillRemoteDS
import com.bosleo.feedback.data.skill.datasource.remote.SkillRemoteSource
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillLocalDS
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillLocalSource
import com.bosleo.feedback.data.subskill.datasource.remote.SubSkillRemoteDS
import com.bosleo.feedback.data.subskill.datasource.remote.SubSkillRemoteSource
import org.koin.dsl.module.module

val dataSourceModule = module {

    single<CandidateLocalSource> { CandidateLocalDS(get()) }
    single<CandidateRemoteSource> { CandidateRemoteDS(get()) }

    single<LoginRemoteSource> { LoginRemoteDS(get()) }
    single<LoginLocalSource> { LoginLocalDS(get()) }

    single<InterviewRemoteSource> { InterviewRemoteDS(get()) }
    single<InterviewLocalSource> { InterviewLocalDS(get()) }

    single<DepartmentRemoteSource> { DepartmentRemoteDS(get()) }
    single<DepartmentLocalSource> { DepartmentLocalDS(get()) }

    single<FormRemoteSource> { FormRemoteDS(get()) }
    single<FormLocalSource> { FormLocalDS(get()) }

    single<SkillRemoteSource> { SkillRemoteDS(get()) }
    single<SkillLocalSource> { SkillLocalDS(get()) }

    single<SubSkillRemoteSource> { SubSkillRemoteDS(get()) }
    single<SubSkillLocalSource> { SubSkillLocalDS(get()) }

    single<InterviewMarkRemoteSource> { InterviewMarkRemoteDS(get()) }
    single<InterviewMarkLocalSource> { InterviewMarkLocalDS(get()) }

}
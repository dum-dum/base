package com.bosleo.feedback.model

import android.os.Parcelable
import com.bosleo.feedback.api.*
import com.bosleo.feedback.data.candidate.datasource.local.CandidateEntity
import com.bosleo.feedback.data.interview.datasource.local.InterviewEntity
import com.bosleo.feedback.data.marks.datasorce.local.InterviewMarkEntity
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime

data class User(
    val id: String = "",
    val name: String = "",
    val email: String = "",
    val type: UserRole = UserRole.INTERVIEWER
) {
    //fun toUserEntity() = UserEntity(id, name, email, type.name)
}

data class Interview constructor(
    var id: String = "",
    var interviewDate: DateTime = DateTime(),
    var candidateId: String = "",
    var result: String = "",
    var comment: String = "",
    var formId: String = "",
    var signature: String = "",
    var candidate: Candidate? = null
) {
    fun toInterviewEntity(): InterviewEntity =
        InterviewEntity(id, candidateId, interviewDate, comment, result, formId)

    fun toInterviewPRQ() = InterviewPRQ(id, interviewDate, candidateId, result, comment, formId, signature)
}

data class Candidate(
    var id: String = "",
    var experience: String = "",
    var firstName: String = "",
    var lastName: String = "",
    var company: String = "",
    var department: Department
) {
    fun toCandidateEntity() = CandidateEntity(id, experience, department.id, company, firstName, lastName)
    fun toCandidatePRQ(interview: Interview) = CandidatePRQ(
        firstName, lastName, experience, company, id, department.id, interviewPRQ = interview.toInterviewPRQ()
    )
}

data class CandidateWithInterview(
    val candidate: Candidate,
    val interview: Interview
)

data class Department(
    val id: String = "",
    val name: String = ""
) {
    override fun toString(): String {
        return name
    }
}

data class Skill(
    val id: String = "",
    val name: String = "",
    val formId: String = ""
)

data class SubSkill(
    val id: String = "",
    val name: String = "",
    val marks: Int = 0,
    val skillId: String = ""
) {
    fun toSubSkillPRQ() = SubSkillPRQ(skillId, name, marks)
}

data class DepartmentWithForm(
    val id: String,
    val name: String,
    val formWithSkill: FormWithSkill
)

data class FormWithSkill(val form: Form, val skillWithSubSkill: List<SkillWithSubSkill>)

data class SkillWithSubSkill(val skill: Skill, val subSkill: List<SubSkill>)

@Parcelize
data class Form(val id: String, val departmentId: String) : Parcelable {
    var skills = mutableListOf<SkillWithSubSkill>()
}

data class InterviewMark(
    var id: String,
    var skillId: String,
    var interviewId: String,
    var subSkillId: String,
    var mark: Int
) {
    fun toEntity() = InterviewMarkEntity(id, skillId, interviewId, subSkillId, mark)
    fun toFeedback()= FeedbackRS(id,skillId, subSkillId, interviewId, mark)
}

enum class UserRole {
    HR, INTERVIEWER
}
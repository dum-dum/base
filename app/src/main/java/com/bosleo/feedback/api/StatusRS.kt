package com.bosleo.feedback.api

import com.google.gson.annotations.SerializedName

data class StatusRS(

    @SerializedName("status")
    val status: Boolean
)
package com.bosleo.feedback.api

import android.app.Application
import android.content.Intent
import com.bosleo.feedback.presentation.login.LoginActivity
import com.bosleo.feedback.utility.Constants
import com.pixplicity.easyprefs.library.Prefs
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.Response
import okhttp3.ResponseBody

class TokenInterceptor(private val application: Application) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val authorisedRequest = originalRequest!!.newBuilder()
            .header("Authorization", getToken())
            .header("Accept", "application/json")
            .build()

        // Add authorization header with updated authorization value to  intercepted request

        val response = chain.proceed(authorisedRequest)
        if (response.cacheResponse() != null) {
            return Response.Builder().request(response.request())
                .code(304)
                .protocol(response.protocol())
                .message("Not modified")
                .body(ResponseBody.create(MediaType.parse("text/plain"), "{}"))
                .build()

        }

        return when (response.code()) {
            401 -> {
                Prefs.clear()
                application.startActivity(Intent(application, LoginActivity::class.java).also {
                    it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                })
                response
            }

            else -> response
        }


    }

    private fun getToken(): String {
        val token = Prefs.getString(Constants.PrefKey.TOKEN, "")
        return "Token $token"
    }

}
package com.bosleo.feedback.api

import com.bosleo.feedback.data.candidate.datasource.local.CandidateEntity
import com.bosleo.feedback.data.dbjoin.SkillSubSkillJoin
import com.bosleo.feedback.data.form.datasource.local.FormEntity
import com.bosleo.feedback.data.interview.datasource.local.InterviewEntity
import com.bosleo.feedback.data.marks.datasorce.local.InterviewMarkEntity
import com.bosleo.feedback.data.skill.datasource.local.SkillEntity
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillEntity
import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime

data class Post(
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("title")
    val title: String = "",
    @SerializedName("body")
    val body: String = "",
    @SerializedName("userId")
    val userId: Int = 0
)

data class CandidateRS(
    @SerializedName("firstName")
    val firstName: String = "",
    @SerializedName("lastName")
    val lastName: String = "",
    @SerializedName("experience")
    val experience: String = "",
    @SerializedName("company")
    val company: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("departmentId")
    val departmentId: String = "",
    @SerializedName("interview")
    val interview: InterviewRS
) {
    fun toCandidateEntity() = CandidateEntity(id, experience, departmentId, company, firstName, lastName)
}

data class CandidatePRQ(
    @SerializedName("firstName")
    val firstName: String = "",
    @SerializedName("lastName")
    val lastName: String = "",
    @SerializedName("experience")
    val experience: String = "",
    @SerializedName("company")
    val company: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("departmentId")
    val departmentId: String = "",
    @SerializedName("interview")
    val interviewPRQ: InterviewPRQ
)


data class DepartmentRS(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("name")
    val name: String = ""
)

data class AddDepartmentPRQ(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("name")
    val name: String = ""
)

data class FormRS(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("departmentId")
    val departmentId: String = "",
    @SerializedName("skills")
    val skills: List<SkillRS> = arrayListOf()
) {
    fun toFormEntity() = FormEntity(id, departmentId)
}

data class AddFormPRQ(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("departmentId")
    val departmentId: String = ""
)

data class SkillRS(
    @SerializedName("formId")
    val formId: String = "",
    @SerializedName("subSkills")
    val subSkills: List<SubSkillRS>,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("deletedAt")
    val deletedAt: String? = null
) {
    fun toSkillEntity() =
        SkillSubSkillJoin(SkillEntity(id, name, formId, if (deletedAt.isNullOrBlank()) 0 else 1)).also {
            it.skillWithSubSkill = subSkills.map { sub ->
                SubSkillEntity(
                    sub.id,
                    sub.name,
                    sub.marks,
                    sub.skillId,
                    if (sub.deletedAt.isNullOrBlank()) 0 else 1
                )
            }
        }
}


data class SkillPRQ(
    @SerializedName("formId")
    val formId: String = "",
    @SerializedName("name")
    val name: String = ""
)

data class SubSkillPRQ(
    @SerializedName("skillId")
    val skillId: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("marks")
    val marks: Int = 0
)

data class SubSkillDeletePRQ(
    @SerializedName("sub_skill")
    val subSKillId: List<String>
)

data class SubSkillRS(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("skillId")
    val skillId: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("marks")
    val marks: Int = 0,
    @SerializedName("deletedAt")
    val deletedAt: String? = null
) {
    fun toSubSkillEntity() = SubSkillEntity(id, name, marks, skillId, if (deletedAt.isNullOrBlank()) 0 else 1)
}


data class InterviewPRQ constructor(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("date")
    val interviewDate: DateTime = DateTime(),
    @SerializedName("candidateId")
    val candidateId: String = "",
    @SerializedName("result")
    val result: String = "",
    @SerializedName("comment")
    val comment: String = "",
    @SerializedName("formId")
    val formId: String = "",
    @SerializedName("signature")
    val signature: String = ""
)


data class InterviewRS(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("date")
    val date: DateTime = DateTime.now(),
    @SerializedName("candidateId")
    val candidateId: String = "",
    @SerializedName("result")
    val result: String = "",
    @SerializedName("comment")
    val comment: String = "",
    @SerializedName("formId")
    val formId: String = "",
    @SerializedName("signature")
    val signature: String? = null,
    @SerializedName("candidate")
    val candidate: CandidateRS
) {
    fun toInterviewEntity() = InterviewEntity(id, candidateId, date, comment, result, formId)
}

data class LoginPRQ(
    @SerializedName("email")
    val email: String,
    @SerializedName("password")
    val password: String
)


data class FeedbackRS(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("skillId")
    val skillId: String = "",
    @SerializedName("subSkillId")
    val subSkillId: String = "",
    @SerializedName("interviewId")
    val interviewId: String = "",
    @SerializedName("mark")
    val mark: Int = 0
) {
    fun toInterviewMarkEntity()=InterviewMarkEntity(id, skillId, interviewId, subSkillId, mark)
}

data class FeedbackPRQ(
    @SerializedName("feedback")
    val feedback: List<FeedbackRS>
)


package com.bosleo.feedback.api


import com.bosleo.feedback.data.core.database.UserEntity
import com.google.gson.annotations.SerializedName

data class UserRs(
    @SerializedName("id")
    val id: String,
    @SerializedName("username")
    val name: String = "",
    @SerializedName("email")
    val email: String = "",
    @SerializedName("token")
    val token: String = "",
    @SerializedName("role")
    val role: String = "",
    @SerializedName("image")
    val image: String = ""
) {
    //    fun toUser() = User(id, name, email, if (type == "HR") UserRole.HR else UserRole.INTERVIEWER)
    fun toUserEntity() = UserEntity(id, name, email, role.toUpperCase())

}



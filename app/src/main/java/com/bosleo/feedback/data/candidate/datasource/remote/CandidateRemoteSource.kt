package com.bosleo.feedback.data.candidate.datasource.remote

import com.bosleo.feedback.api.CandidatePRQ
import com.bosleo.feedback.api.CandidateRS
import io.reactivex.Single

interface CandidateRemoteSource {

    fun insertCandidate(candidate: CandidatePRQ): Single<CandidateRS>
    fun getAllCandidate(): Single<List<CandidateRS>>
}
package com.bosleo.feedback.data.candidate.datasource.remote

import com.bosleo.feedback.api.CandidatePRQ
import com.bosleo.feedback.api.CandidateRS
import com.bosleo.feedback.api.StatusRS
import io.reactivex.Single

class CandidateRemoteDS constructor(private val candidateApiService: CandidateApiService) : CandidateRemoteSource {
    override fun getAllCandidate() = candidateApiService.getAllCandidate()

    override fun insertCandidate(candidate: CandidatePRQ): Single<CandidateRS> {
        return candidateApiService.insertCandidate(candidate)
    }

}
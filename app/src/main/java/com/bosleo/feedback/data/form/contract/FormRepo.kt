package com.bosleo.feedback.data.form.contract

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.dbjoin.FormSkillJoin
import com.bosleo.feedback.model.Form
import com.bosleo.feedback.utility.Either

interface FormRepo {
    suspend fun insertForm(departmentId: String): Either<Exception, Form>
    suspend fun getForm(departmentId: String): Either<Exception, Form>
    suspend fun getFormWithSkills(formid: String): Either<Exception, LiveData<Form>>
}
package com.bosleo.feedback.data.marks.datasorce.local

class InterviewMarkLocalDS constructor(
    private val interviewMarksDao: InterviewMarksDao
) : InterviewMarkLocalSource {

    override fun getInterviewMarks(interviewId: String) = interviewMarksDao.getInterviewMarks(interviewId)

    override fun adMark(markEntity: InterviewMarkEntity): InterviewMarkEntity {
        return if (interviewMarksDao.insert(markEntity) > 0) {
            interviewMarksDao.getInterviewMarkByID(markEntity.id)
        } else {
            throw Exception()
        }
    }

    override fun addMark(markEntity: List<InterviewMarkEntity>) = interviewMarksDao.insert(markEntity)

    override fun updateMark(markEntity: InterviewMarkEntity) = interviewMarksDao.update(markEntity)

}
package com.bosleo.feedback.data.skill.datasource.remote

import com.bosleo.feedback.api.SkillPRQ
import com.bosleo.feedback.api.SkillRS
import com.google.gson.JsonObject
import io.reactivex.Single

interface SkillRemoteSource {

    fun fetchAllSkills(): Single<List<SkillRS>>

    fun addSkill(skillPRQ: SkillPRQ): Single<SkillRS>

    fun deleteSkill(skillId: String): Single<JsonObject>

}
package com.bosleo.feedback.data.skill.contract

import androidx.lifecycle.LiveData
import com.bosleo.feedback.model.Skill
import com.bosleo.feedback.utility.Either

interface SkillRepo {

    suspend fun getAllSkills(): Either<Exception, LiveData<List<Skill>>>

    suspend fun addSkill(skill: Skill): Either<Exception, Skill>

    suspend fun deleteSkill(skillId: String): Either<Exception,Boolean>
}
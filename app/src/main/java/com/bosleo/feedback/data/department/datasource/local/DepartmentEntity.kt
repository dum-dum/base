package com.bosleo.feedback.data.department.datasource.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bosleo.feedback.data.core.database.TableContract

@Entity(tableName = TableContract.Department.DEPARTMENT)
data class DepartmentEntity(
    @PrimaryKey
    @ColumnInfo(name = TableContract.Department.ID)
    var id: String,
    @ColumnInfo(name = TableContract.Department.NAME)
    var name: String
)
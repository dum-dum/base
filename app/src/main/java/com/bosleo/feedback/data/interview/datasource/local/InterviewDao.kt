package com.bosleo.feedback.data.interview.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.bosleo.feedback.data.core.BaseDao
import com.bosleo.feedback.data.core.database.TableContract
import com.bosleo.feedback.data.dbjoin.CandidateInterviewJoin

@Dao
interface InterviewDao : BaseDao<InterviewEntity> {

    @Query("SELECT * FROM ${TableContract.Candidate.CANDIDATE}")
    fun getAllInterviews(): LiveData<List<CandidateInterviewJoin>>

    @Query("SELECT * FROM `interview` WHERE ${TableContract.Interview.ID}=:interviewId")
    fun getInterviewById(interviewId: String): LiveData<InterviewEntity>

}
package com.bosleo.feedback.data.department.contract

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.bosleo.feedback.api.AddDepartmentPRQ
import com.bosleo.feedback.data.core.BaseDataRepo
import com.bosleo.feedback.data.department.datasource.local.DepartmentEntity
import com.bosleo.feedback.data.department.datasource.local.DepartmentLocalSource
import com.bosleo.feedback.data.department.datasource.remote.DepartmentRemoteSource
import com.bosleo.feedback.data.form.datasource.local.FormLocalSource
import com.bosleo.feedback.data.form.datasource.remote.FormRemoteSource
import com.bosleo.feedback.model.*
import com.bosleo.feedback.utility.Either
import com.bosleo.feedback.utility.generateIdIfBlank
import io.reactivex.internal.operators.single.SingleFromCallable
import kotlinx.coroutines.rx2.await
import java.lang.RuntimeException

class DepartmentDataRepo(
    private val departmentLocalSource: DepartmentLocalSource,
    private val departmentRemoteSource: DepartmentRemoteSource
) : BaseDataRepo(), DepartmentRepo {

    override suspend fun getDepartmentWithForm(departmentId: String): Either<Exception, DepartmentWithForm> {
        return either {
            val departmentWithFormJoin = departmentLocalSource.getDepartmentWithFormJoin(departmentId)
            val skillWithSubSkill = departmentWithFormJoin.departmentWithForm.first().formWithSkill.map {
                SkillWithSubSkill(
                    Skill(it.skillEntity.id, it.skillEntity.name, it.skillEntity.formId),
                    it.skillWithSubSkill.map {
                        SubSkill(it.id, it.name, it.marks, it.skillId)
                    })
            }
            val formEntity = departmentWithFormJoin.departmentWithForm.first().formEntity
            val formWithSkill =
                FormWithSkill(Form(formEntity.id, formEntity.departmentId), skillWithSubSkill)
            val departmentWithForm = DepartmentWithForm(
                departmentWithFormJoin.departmentEntity.id,
                departmentWithFormJoin.departmentEntity.name,
                formWithSkill
            )
            departmentWithForm
        }
    }

    override suspend fun insertDepartment(department: Department): Either<Exception, Department> {
        return either {
            val id = department.id.generateIdIfBlank()
            val departmentRs = departmentRemoteSource.insertDepartment(AddDepartmentPRQ(id, department.name)).await()
            if (departmentRs!=null) {
                departmentLocalSource.addDepartment(DepartmentEntity(departmentRs.id, departmentRs.name))
                Department(id, department.name)
            } else {
                throw AppException.ServerException(RuntimeException("Unable to add try again latter"))

            }
        }
    }

    override suspend fun getAllDepartments(): Either<Exception, LiveData<List<Department>>> {
        return either({
            Transformations.map(departmentLocalSource.getAllDepartments()) {
                it.map {
                    Department(it.id, it.name)
                }
            }
        }, {
            departmentRemoteSource.fetchAllDepartments().await().map {
                departmentLocalSource.addDepartment(with(it) {
                    DepartmentEntity(id, name)
                })
            }
        })
    }

}
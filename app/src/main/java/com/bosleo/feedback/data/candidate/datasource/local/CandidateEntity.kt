package com.bosleo.feedback.data.candidate.datasource.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bosleo.feedback.data.core.database.TableContract
import com.bosleo.feedback.model.Candidate
import com.bosleo.feedback.model.Department

@Entity(tableName = TableContract.Candidate.CANDIDATE)
data class CandidateEntity constructor(
    @PrimaryKey
    @ColumnInfo(name = TableContract.Candidate.ID)
    var id: String,
    @ColumnInfo(name = TableContract.Candidate.EXPERIENCE)
    var experience: String,
    @ColumnInfo(name = TableContract.Candidate.DEPARTMENT_ID)
    var departmentId: String,
    @ColumnInfo(name = TableContract.Candidate.COMPANY)
    var company: String,
    @ColumnInfo(name = TableContract.Candidate.FIRST_NAME)
    var firstName: String,
    @ColumnInfo(name = TableContract.Candidate.LAST_NAME)
    var lastName: String
) {
    fun toCandidate() = Candidate(id, experience, firstName, lastName, department = Department())
}
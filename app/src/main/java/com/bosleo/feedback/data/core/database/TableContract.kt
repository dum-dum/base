package com.bosleo.feedback.data.core.database

object TableContract {

    object Candidate {
        const val CANDIDATE = "CANDIDATE"
        const val ID = "ID"
        const val EXPERIENCE = "EXPERIENCE"
        const val DEPARTMENT_ID = "DEPARTMENT_ID"
        const val COMPANY = "COMPANY"
        const val DATE = "DATE"
        const val FIRST_NAME = "FIRST_NAME"
        const val LAST_NAME = "LAST_NAME"
    }


    object Department {
        const val DEPARTMENT = "DEPARTMENT"
        const val NAME = "NAME"
        const val ID = "ID"

    }

    object Form {
        const val FORM = "FORM"
        const val ID = "ID"
        const val DEPARTMENT_ID = "DEPARTMENT_ID"
    }

    object Skill {
        const val SKILL = "SKILL"
        const val FORM_ID = "FORM_ID"
        const val ID = "ID"
        const val NAME = "NAME"
        const val IS_DELETED="IS_DELETED"
    }

    object SubSkill {
        const val SUB_SKILL = "SUB_SKILL"
        const val SKILL_ID = "SKILL_ID"
        const val ID = "ID"
        const val NAME = "NAME"
        const val MARKS = "MARKS"
        const val IS_DELETED="IS_DELETED"
    }


    object Interview {
        const val INTERVIEW = "INTERVIEW"
        const val ID = "ID"
        const val CANDIDATE_ID = "CANDIDATE_ID"
        const val DATE = "DATE"
        const val INTERVIEWER_COMMENTS = "INTERVIEWER_COMMENTS"
        const val RESULT = "RESULT"
        const val FORM_ID = "FORM_ID"
    }

    object InterviewMark {
        const val INTERVIEW_MARK = "INTERVIEW_MARK"
        const val ID = "ID"
        const val INTERVIEW_ID = "INTERVIEW_ID"
        const val SKILL_ID = "SKILL_ID"
        const val SUB_SKILL_ID = "SUB_SKILL_ID"
        const val MARK = "MARK"
    }

    object MarkTransaction {
        const val MARKS_TRANSACTION = "MARKS_TRANSACTION"
        const val ID = "ID"
        const val JSON = "JSON"
        const val status = "STATUS"
    }

    object User {
        const val USER = "USER"
        const val ID = "ID"
        const val NAME = "NAME"
        const val TYPE = "TYPE"
        const val EMAIL = "EMAIL"
    }
}
package com.bosleo.feedback.data.syncmaster

import io.reactivex.Single

interface MasterRepo {

    fun fetchMasters(): Single<List<Long>>

}
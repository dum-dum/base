package com.bosleo.feedback.data.marks.datasorce.local

interface InterviewMarkLocalSource {

    fun adMark(markEntity: InterviewMarkEntity): InterviewMarkEntity

    fun addMark(markEntity: List<InterviewMarkEntity>): List<Long>

    fun updateMark(markEntity: InterviewMarkEntity): Int

    fun getInterviewMarks(interviewId: String): List<InterviewMarkEntity>
}
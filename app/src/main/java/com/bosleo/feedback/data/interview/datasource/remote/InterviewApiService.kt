package com.bosleo.feedback.data.interview.datasource.remote

import com.bosleo.feedback.api.InterviewPRQ
import com.bosleo.feedback.api.InterviewRS
import com.bosleo.feedback.api.StatusRS
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

interface InterviewApiService {

    @GET("interview")
    fun fetchAllInterviews(): Single<List<InterviewRS>>



    @PUT("interview")
    fun putInterview(@Body interviewPRQ: InterviewPRQ): Single<InterviewRS>


    @POST("interview")
    fun postInterview(@Body interviewPRQ: InterviewPRQ): Single<InterviewRS>
}
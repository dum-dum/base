package com.bosleo.feedback.data.department.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.bosleo.feedback.data.core.BaseDao
import com.bosleo.feedback.data.core.database.TableContract
import com.bosleo.feedback.data.dbjoin.DepartmentFormJoin
import io.reactivex.Single

@Dao
interface DepartmentDao : BaseDao<DepartmentEntity> {

    @Query("SELECT * FROM ${TableContract.Department.DEPARTMENT}")
    fun getAllDepartments(): LiveData<List<DepartmentEntity>>

    @Query("SELECT * FROM ${TableContract.Department.DEPARTMENT} WHERE ${TableContract.Department.ID} =:departmentId")
    fun getDepartmentWithForm(departmentId:String):DepartmentFormJoin
}
package com.bosleo.feedback.data.form.contract

import androidx.lifecycle.Transformations
import com.bosleo.feedback.data.core.BaseDataRepo
import com.bosleo.feedback.data.form.datasource.local.FormEntity
import com.bosleo.feedback.data.form.datasource.local.FormLocalSource
import com.bosleo.feedback.data.form.datasource.remote.FormRemoteSource
import com.bosleo.feedback.data.skill.datasource.local.SkillLocalSource
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillLocalSource
import com.bosleo.feedback.model.Form
import com.bosleo.feedback.utility.Either
import com.bosleo.feedback.utility.generateIdIfBlank
import kotlinx.coroutines.rx2.await

class FormDataRepo(
    private val formLocalSource: FormLocalSource,
    private val formRemoteSource: FormRemoteSource,
    private val skillLocalSource: SkillLocalSource,
    private val subSkillLocalSource: SubSkillLocalSource

) : FormRepo, BaseDataRepo() {

    override suspend fun insertForm(departmentId: String) = either {
        return@either try {
            formRemoteSource.fetchForm().await().forEach {
                formLocalSource.addForm(it.toFormEntity())
            }
            formLocalSource.getForm(departmentId).toForm()
        } catch (e: Exception) {
            val id: String = "".generateIdIfBlank()
            formRemoteSource.addForm(id, departmentId).await()
            formLocalSource.addForm(FormEntity(id, departmentId))
            formLocalSource.getForm(departmentId).toForm()
        }

    }

    override suspend fun getForm(departmentId: String): Either<Exception, Form> = either({
        formLocalSource.getForm(departmentId).toForm()
    }, {
        formRemoteSource.fetchForm().await().map { it.toFormEntity() }.apply {
            formLocalSource.addForm(this)
        }
    })

    override suspend fun getFormWithSkills(formid: String) = either({
        Transformations.map(
            formLocalSource.getFormWithSkillById(formid)
        ) {
            it.formEntity.toForm().also { form ->
                form.skills = it.formWithSkill.filter {
                    it.skillEntity.isDeleted==0
                }.map { skill ->
                    skill.toSkillWithSubSkill()
                }.toMutableList()
            }

        }

    }, {
        formRemoteSource.getFormById(formid).await()?.apply {
            formLocalSource.addForm(toFormEntity())
            skills.forEach {
                it.toSkillEntity().let { skillSubSkillJoin ->

                    skillLocalSource.addSkill(skillSubSkillJoin.skillEntity)
                    subSkillLocalSource.addSubSkills(skillSubSkillJoin.skillWithSubSkill)

                }

            }

        }
    })
}
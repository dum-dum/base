package com.bosleo.feedback.data.login.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.bosleo.feedback.data.core.BaseDao
import com.bosleo.feedback.data.core.database.TableContract
import com.bosleo.feedback.data.core.database.UserEntity
@Dao
interface UserDao : BaseDao<UserEntity> {
    @Query("Select * FROM ${TableContract.User.USER}")
    fun getUser(): LiveData<UserEntity>
}
package com.bosleo.feedback.data.login.datasource.remote

import com.bosleo.feedback.api.LoginPRQ

class LoginRemoteDS constructor(private val loginApiService: LoginApiService) : LoginRemoteSource {

    override fun doLogin(email: String, password: String) = loginApiService.login(LoginPRQ(email, password))

}
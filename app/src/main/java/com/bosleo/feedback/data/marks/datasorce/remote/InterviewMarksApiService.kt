package com.bosleo.feedback.data.marks.datasorce.remote

import com.bosleo.feedback.api.FeedbackPRQ
import com.bosleo.feedback.api.FeedbackRS
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface InterviewMarksApiService {

    @POST("feedback")
    fun submitMarks(@Body feedbackPRQ: FeedbackPRQ): Single<List<FeedbackRS>>

    @GET("feedback/{interview_id}")
    fun fetchFeedbackByInterviewId(@Path("interview_id") interviewId: String): Single<List<FeedbackRS>>
}
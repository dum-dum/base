package com.bosleo.feedback.data.core

import com.bosleo.feedback.utility.Either

abstract class BaseDataRepo {

    inline fun <R> either(execution: () -> R): Either<Exception, R> {
        return try {
            Either.Right(execution())
        } catch (e: Exception) {
            Either.Left(Exception(e.localizedMessage))
        }
    }

    inline fun <R> either(execution: () -> R, executeIfConnected: () -> Unit): Either<Exception, R> {
        try {
            executeIfConnected()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return try {
            Either.Right(execution())
        } catch (e: Exception) {
            Either.Left(Exception(e.localizedMessage))
        }
    }


    sealed class AppException(val throwable: Throwable) : Exception() {
        class ServerException(throwable: Throwable) : AppException(throwable)
    }
}
package com.bosleo.feedback.data.subskill.datasource.local

import androidx.lifecycle.LiveData

class SubSkillLocalDS(private val subSkillDao: SubSkillDao) : SubSkillLocalSource {

    override fun getAllSubSkills(): LiveData<List<SubSkillEntity>> {
        return subSkillDao.getAllSubSkills()
    }

    override fun addSubSkill(subSkillEntity: SubSkillEntity): Long {
        return subSkillDao.insert(subSkillEntity)
    }

    override fun addSubSkills(subSkillEntities: List<SubSkillEntity>): List<Long> {
        return subSkillDao.insert(subSkillEntities)
    }

    override fun deleteSubSkills(subSkillIds: List<String>): Boolean {
        return subSkillDao.deleteSubSkills(subSkillIds)>0
    }
}
package com.bosleo.feedback.data.department.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.dbjoin.DepartmentFormJoin

class DepartmentLocalDS(private val departmentDao: DepartmentDao) : DepartmentLocalSource {

    override fun getAllDepartments(): LiveData<List<DepartmentEntity>> {
        return departmentDao.getAllDepartments()
    }

    override fun addDepartment(departmentEntity: DepartmentEntity): Long {
        return departmentDao.insert(departmentEntity)
    }

    override fun getDepartmentWithFormJoin(departmentId: String): DepartmentFormJoin {
        return departmentDao.getDepartmentWithForm(departmentId)
    }
}
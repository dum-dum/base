package com.bosleo.feedback.data.candidate.contract

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.bosleo.feedback.data.candidate.datasource.local.CandidateEntity
import com.bosleo.feedback.data.candidate.datasource.local.CandidateLocalSource
import com.bosleo.feedback.data.candidate.datasource.remote.CandidateRemoteSource
import com.bosleo.feedback.data.core.BaseDataRepo
import com.bosleo.feedback.data.interview.datasource.local.InterviewLocalSource
import com.bosleo.feedback.model.Candidate
import com.bosleo.feedback.model.CandidateWithInterview
import com.bosleo.feedback.model.Department
import com.bosleo.feedback.model.Interview
import com.bosleo.feedback.utility.Either
import com.bosleo.feedback.utility.generateIdIfBlank
import kotlinx.coroutines.rx2.await

class CandidateDataRepo(
    private val candidateLocalSource: CandidateLocalSource,
    private val candidateRemoteSource: CandidateRemoteSource,
    private val interviewLocalSource: InterviewLocalSource
) : BaseDataRepo(), CandidateRepo {
    override suspend fun findCandidate(name: String): Either<Exception, LiveData<Boolean>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun addCandidate(candidate: Candidate, interview: Interview): Either<Exception, Candidate> {
        return either {
            candidateRemoteSource.insertCandidate(candidate.toCandidatePRQ(interview)).await()
                .apply {
                    candidateLocalSource.addCandidate(toCandidateEntity())
                    interviewLocalSource.addInterview(interview.toInterviewEntity())
                }

            candidate
        }
    }

    override suspend fun getAllCandidate() = either({
        Transformations.map(candidateLocalSource.getAllCandidate()) {
            it.map { map ->
                map.candidateEntity.toCandidate().also {
                    it.department =
                            Department(map.candidateWithDepartment.first().id, map.candidateWithDepartment.first().name)
                }
            }
        }
    }, {
        candidateRemoteSource.getAllCandidate().map {
            it.forEach {
                candidateLocalSource.addCandidate(with(it) {
                    CandidateEntity(id, experience, departmentId, company, firstName, lastName)
                })
            }

        }.await()
    })

    override suspend fun findCandidateById(id: String) = either {

        with(candidateLocalSource.getCandidateById(id))
        {
            CandidateWithInterview(
                candidateEntity.toCandidate().also {
                    it.department = with(this.candidateWithDepartment.first()) {
                        Department(id, name)
                    }
                },
                this.candidateWithInterview.first().toInterview()
            )
        }
    }

}
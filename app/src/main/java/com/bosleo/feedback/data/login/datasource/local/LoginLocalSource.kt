package com.bosleo.feedback.data.login.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.core.database.UserEntity

interface LoginLocalSource {

    fun insertUser(userEntity: UserEntity): Long
    fun getUser(): LiveData<UserEntity>
}
package com.bosleo.feedback.data.department.datasource.remote

import com.bosleo.feedback.api.AddDepartmentPRQ
import com.bosleo.feedback.api.DepartmentRS
import com.bosleo.feedback.api.StatusRS
import io.reactivex.Single

class DepartmentRemoteDS(private val departmentApiService: DepartmentApiService) : DepartmentRemoteSource {

    override fun fetchAllDepartments(): Single<List<DepartmentRS>> = departmentApiService.getAllDepartments()

    override fun insertDepartment(addDepartmentPRQ: AddDepartmentPRQ) =departmentApiService.insertDepartment(addDepartmentPRQ)
}
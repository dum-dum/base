package com.bosleo.feedback.data.login.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.core.database.UserEntity

class LoginLocalDS constructor(private val userDao: UserDao) : LoginLocalSource {

    override fun insertUser(userEntity: UserEntity): Long = userDao.insert(userEntity)

    override fun getUser(): LiveData<UserEntity> = userDao.getUser()

}
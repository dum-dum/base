package com.bosleo.feedback.data.marks.contract

import com.bosleo.feedback.api.FeedbackPRQ
import com.bosleo.feedback.data.core.BaseDataRepo
import com.bosleo.feedback.data.marks.datasorce.local.InterviewMarkLocalSource
import com.bosleo.feedback.data.marks.datasorce.remote.InterviewMarkRemoteSource
import com.bosleo.feedback.model.InterviewMark
import com.bosleo.feedback.utility.generateIdIfBlank
import kotlinx.coroutines.rx2.await

class InterviewMarksDataRepo constructor(
    private val markLocalSource: InterviewMarkLocalSource,
    private val markRemoteSource: InterviewMarkRemoteSource
) : BaseDataRepo(), InterviewMarksRepo {

    override suspend fun addMark(interviewMark: InterviewMark) = either {
        if (interviewMark.id.isBlank()) {
            interviewMark.id = interviewMark.id.generateIdIfBlank()
            markLocalSource.adMark(
                interviewMark.toEntity()
            )
        } else {
            markLocalSource.updateMark(interviewMark.toEntity())
        }
        interviewMark
    }

    override suspend fun addMark(interviewMarks: List<InterviewMark>) = either {
        markRemoteSource.addUpdateFeedback(
            FeedbackPRQ(
                interviewMarks.map { it.toFeedback() })
        ).map {
            it.map { feedbackRS ->
                markLocalSource.adMark(feedbackRS.toInterviewMarkEntity())
            }.map { interviewMarkEntity ->
                interviewMarkEntity.toInterviewMarks()
            }
        }.await()
//        interviewMarks.asSequence()
//            .map { interviewMark ->
//                if (interviewMark.id.isBlank()) {
//                    interviewMark.id = interviewMark.id.generateIdIfBlank()
//                    markLocalSource.adMark(
//                        interviewMark.toEntity()
//                    )
//
//                } else {
//                    markLocalSource.updateMark(interviewMark.toEntity())
//                }
//                interviewMark
//            }.toList()

    }

    override suspend fun getMarksForInterView(interviewID: String) = either({
        markLocalSource.getInterviewMarks(interviewID)
            .map { it.toInterviewMarks() }
    }, {
        markRemoteSource.fetchFeedbackByInterviewID(interviewID)
            .map {
                it.map { feedbackRS ->
                    markLocalSource.adMark(feedbackRS.toInterviewMarkEntity())
                }.map { interviewMarkEntity -> interviewMarkEntity.toInterviewMarks() }
            }.await()
    })

}
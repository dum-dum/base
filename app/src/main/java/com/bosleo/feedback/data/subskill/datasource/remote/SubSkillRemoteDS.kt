package com.bosleo.feedback.data.subskill.datasource.remote

import com.bosleo.feedback.api.SubSkillDeletePRQ
import com.bosleo.feedback.api.SubSkillPRQ
import com.bosleo.feedback.api.SubSkillRS
import com.google.gson.JsonObject
import io.reactivex.Single

class SubSkillRemoteDS(private val subSkillApiService: SubSkillApiService) : SubSkillRemoteSource {

    override fun fetchAllSubSkills(): Single<List<SubSkillRS>> {
        return subSkillApiService.fetchAllSubSkills()
    }
    override fun addSubSkill(subSkillPRQ: SubSkillPRQ) = subSkillApiService.addSubSkill(subSkillPRQ.skillId,subSkillPRQ)

    override fun deleteSubSkill(skillId: String, subSkillIds: List<String>): Single<JsonObject> {
        return subSkillApiService.deleteSubSkillId(skillId, SubSkillDeletePRQ( subSkillIds))
    }
}
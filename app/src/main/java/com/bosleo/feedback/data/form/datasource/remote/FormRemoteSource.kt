package com.bosleo.feedback.data.form.datasource.remote

import com.bosleo.feedback.api.FormRS
import com.bosleo.feedback.api.StatusRS
import io.reactivex.Single

interface FormRemoteSource {

    fun fetchForm(): Single<List<FormRS>>

    fun addForm(id:String,departmentId: String): Single<FormRS>

    fun getFormById(id: String):Single<FormRS>
}
package com.bosleo.feedback.data.department.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.dbjoin.DepartmentFormJoin

interface DepartmentLocalSource {

    fun getAllDepartments(): LiveData<List<DepartmentEntity>>

    fun addDepartment(departmentEntity: DepartmentEntity): Long

    fun getDepartmentWithFormJoin(departmentId:String):DepartmentFormJoin

}
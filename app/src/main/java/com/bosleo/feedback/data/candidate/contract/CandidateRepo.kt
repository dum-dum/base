package com.bosleo.feedback.data.candidate.contract

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.candidate.datasource.local.CandidateEntity
import com.bosleo.feedback.model.Candidate
import com.bosleo.feedback.model.CandidateWithInterview
import com.bosleo.feedback.model.Interview
import com.bosleo.feedback.utility.Either

interface CandidateRepo {
    suspend fun getAllCandidate(): Either<Exception, LiveData<List<Candidate>>>
    suspend fun addCandidate(candidate: Candidate,interview: Interview): Either<Exception, Candidate>
    suspend fun findCandidate(name: String): Either<Exception, LiveData<Boolean>>
    suspend fun findCandidateById(id: String): Either<Exception, CandidateWithInterview>
}
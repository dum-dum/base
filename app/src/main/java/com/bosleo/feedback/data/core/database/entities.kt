package com.bosleo.feedback.data.core.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bosleo.feedback.model.User
import com.bosleo.feedback.model.UserRole


//@Entity(tableName = TableContract.Department.DEPARTMENT)
//data class DepartmentEntity(
//
//    @PrimaryKey
//    @ColumnInfo(name = TableContract.Department.ID)
//    var id: String,
//    @ColumnInfo(name = TableContract.Department.NAME)
//    var name: String
//)

//@Entity(tableName = TableContract.Form.FORM)
//data class FormEntity(
//
//    @PrimaryKey
//    @ColumnInfo(name = TableContract.Form.ID)
//    var id: String,
//    @ColumnInfo(name = TableContract.Form.DEPARTMENT_ID)
//    var departmentId: String
//)

//@Entity(tableName = TableContract.Skill.SKILL)
//data class SkillEntity(
//
//    @PrimaryKey
//    @ColumnInfo(name = TableContract.Skill.ID)
//    var id: String,
//    @ColumnInfo(name = TableContract.Skill.FORM_ID)
//    var formId: String
//)


//@Entity(tableName = TableContract.SubSkill.SUB_SKILL)
//data class SubSkillEntity(
//
//    @PrimaryKey
//    @ColumnInfo(name = TableContract.SubSkill.ID)
//    var id: String,
//    @ColumnInfo(name = TableContract.SubSkill.SKILL_ID)
//    var skillId: String
//)


//@Entity(tableName = TableContract.Interview.INTERVIEW)
//data class InterviewEntity(
//
//    @PrimaryKey
//    @ColumnInfo(name = TableContract.Interview.ID)
//    var id: String,
//    @ColumnInfo(name = TableContract.Interview.CANDIDATE_ID)
//    var candidateId: String,
//    @ColumnInfo(name = TableContract.Interview.DATE)
//    var date: String,
//    @ColumnInfo(name = TableContract.Interview.INTERVIEWER_COMMENTS)
//    var interviewerComments: String,
//    @ColumnInfo(name = TableContract.Interview.RESULT)
//    var result: String,
//    @ColumnInfo(name = TableContract.Interview.FORM_ID)
//    var formId: String
//)


//@Entity(tableName = TableContract.InterviewMark.INTERVIEW_MARK)
//data class InterviewMarkEntity(
//    @PrimaryKey
//    @ColumnInfo(name = TableContract.InterviewMark.ID)
//    var id: String,
//    @ColumnInfo(name = TableContract.InterviewMark.SKILL_ID)
//    var skillId: String,
//    @ColumnInfo(name = TableContract.InterviewMark.INTERVIEW_ID)
//    var interviewId: String,
//    @ColumnInfo(name = TableContract.InterviewMark.SUB_SKILL_ID)
//    var subSkillId: String,
//    @ColumnInfo(name = TableContract.InterviewMark.MARK)
//    var mark: Int
//)

@Entity(tableName = TableContract.User.USER)
data class UserEntity(
    @PrimaryKey
    @ColumnInfo(name = TableContract.User.ID)
    var id: String,
    @ColumnInfo(name = TableContract.User.NAME)
    var name: String,
    @ColumnInfo(name = TableContract.User.EMAIL)
    var email: String,
    @ColumnInfo(name = TableContract.User.TYPE)
    var type: String
) {
    fun toUser() = User(id, name, email, if (type == "HR") UserRole.HR else UserRole.INTERVIEWER)
}
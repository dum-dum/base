package com.bosleo.feedback.data.interview.datasource.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bosleo.feedback.data.core.database.TableContract
import com.bosleo.feedback.model.Candidate
import com.bosleo.feedback.model.Department
import com.bosleo.feedback.model.Interview
import org.joda.time.DateTime

@Entity(tableName = TableContract.Interview.INTERVIEW)
data class InterviewEntity constructor(
    @PrimaryKey
    @ColumnInfo(name = TableContract.Interview.ID)
    var id: String,
    @ColumnInfo(name = TableContract.Interview.CANDIDATE_ID)
    var candidateId: String,
    @ColumnInfo(name = TableContract.Interview.DATE)
    var date: DateTime,
    @ColumnInfo(name = TableContract.Interview.INTERVIEWER_COMMENTS)
    var interviewerComments: String,
    @ColumnInfo(name = TableContract.Interview.RESULT)
    var result: String,
    @ColumnInfo(name = TableContract.Interview.FORM_ID)
    var formId: String
) {
    fun toInterview() =
        Interview(
            id,
            DateTime(date),
            candidateId,
            result,
            interviewerComments,
            formId,
            "",
            Candidate(department = Department())
        )
}






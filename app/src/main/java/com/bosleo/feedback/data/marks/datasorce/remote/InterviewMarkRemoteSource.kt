package com.bosleo.feedback.data.marks.datasorce.remote

import com.bosleo.feedback.api.FeedbackPRQ
import com.bosleo.feedback.api.FeedbackRS
import io.reactivex.Single

interface InterviewMarkRemoteSource {
    fun addUpdateFeedback(feedbackPRQ: FeedbackPRQ): Single<List<FeedbackRS>>
    fun fetchFeedbackByInterviewID(interviewID: String): Single<List<FeedbackRS>>
}
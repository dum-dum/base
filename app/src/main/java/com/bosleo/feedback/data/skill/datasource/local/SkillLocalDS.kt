package com.bosleo.feedback.data.skill.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.model.Skill

class SkillLocalDS(private val skillDao: SkillDao) : SkillLocalSource {

    override fun getAllSkills(): LiveData<List<SkillEntity>> {
        return skillDao.getAllSkills()
    }

    override fun addSkill(skillEntity: SkillEntity): Long {
        return skillDao.insert(skillEntity)
    }

    override fun deleteSkill(skillId: String): Boolean {
        return skillDao.deleteSkill(skillId)>0
    }
}
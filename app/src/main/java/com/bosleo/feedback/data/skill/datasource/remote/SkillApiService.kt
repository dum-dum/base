package com.bosleo.feedback.data.skill.datasource.remote

import com.bosleo.feedback.api.SkillPRQ
import com.bosleo.feedback.api.SkillRS
import com.google.gson.JsonObject
import io.reactivex.Single
import retrofit2.http.*

interface SkillApiService {

    @GET("skill")
    fun fetchAllSkills(): Single<List<SkillRS>>

    @POST("skill")
    fun addSkill(@Body skillPRQ: SkillPRQ): Single<SkillRS>

    @HTTP(method = "DELETE", path = "skill/{skill_id}")
    fun deleteSkill(@Path("skill_id") skillId: String): Single<JsonObject>


}
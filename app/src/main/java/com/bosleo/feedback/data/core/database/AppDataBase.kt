package com.bosleo.feedback.data.core.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.bosleo.feedback.data.candidate.datasource.local.CandidateDao
import com.bosleo.feedback.data.candidate.datasource.local.CandidateEntity
import com.bosleo.feedback.data.department.datasource.local.DepartmentDao
import com.bosleo.feedback.data.department.datasource.local.DepartmentEntity
import com.bosleo.feedback.data.form.datasource.local.FormDao
import com.bosleo.feedback.data.form.datasource.local.FormEntity
import com.bosleo.feedback.data.interview.datasource.local.InterviewDao
import com.bosleo.feedback.data.interview.datasource.local.InterviewEntity
import com.bosleo.feedback.data.login.datasource.local.UserDao
import com.bosleo.feedback.data.marks.datasorce.local.InterviewMarkEntity
import com.bosleo.feedback.data.marks.datasorce.local.InterviewMarksDao
import com.bosleo.feedback.data.skill.datasource.local.SkillDao
import com.bosleo.feedback.data.skill.datasource.local.SkillEntity
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillDao
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillEntity

@Database(
    entities = [
        CandidateEntity::class,
        DepartmentEntity::class,
        FormEntity::class,
        SkillEntity::class,
        SubSkillEntity::class,
        InterviewEntity::class,
        InterviewMarkEntity::class,
        UserEntity::class
    ], version = 1
)
@TypeConverters(value = [DateToLongConverter::class])
abstract class AppDataBase : RoomDatabase() {

    abstract fun candidateDao(): CandidateDao
    abstract fun userDao(): UserDao
    abstract fun departmentDao(): DepartmentDao
    abstract fun interviewDao(): InterviewDao
    abstract fun formDao(): FormDao
    abstract fun skillDao(): SkillDao
    abstract fun subSkillDao(): SubSkillDao
    abstract fun interviewMarkDao(): InterviewMarksDao

}
package com.bosleo.feedback.data.skill.datasource.local

import androidx.lifecycle.LiveData

interface SkillLocalSource {

    fun getAllSkills(): LiveData<List<SkillEntity>>

    fun addSkill(skillEntity: SkillEntity): Long

    fun deleteSkill(skillId: String): Boolean

}
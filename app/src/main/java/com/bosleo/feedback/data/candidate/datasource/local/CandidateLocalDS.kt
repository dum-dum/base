package com.bosleo.feedback.data.candidate.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.dbjoin.CandidateInterviewJoin

class CandidateLocalDS constructor(private val candidateDao: CandidateDao) : CandidateLocalSource {
    override fun getCandidateById(candidateId: String) = candidateDao.getCandidateById(candidateId)

    override fun getAllCandidate(): LiveData<List<CandidateInterviewJoin>> = candidateDao.getAllCandidate()

    override fun addCandidate(candidateEntity: CandidateEntity): Long = candidateDao.insert(candidateEntity)
}
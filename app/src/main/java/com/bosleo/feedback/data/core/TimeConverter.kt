package com.bosleo.feedback.data.core

import com.google.gson.*
import org.joda.time.LocalTime
import org.joda.time.format.DateTimeFormat
import java.lang.reflect.Type

class TimeConverter : JsonSerializer<LocalTime>, JsonDeserializer<LocalTime> {
    override fun serialize(src: LocalTime, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return JsonPrimitive(src.toString("HH:mm:ss"))
    }

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): LocalTime? {
        if (json.asString == null || json.asString.isEmpty()) {
            return null
        }

        try {
            val formatter = DateTimeFormat.forPattern("HH:mm:ss")
            return formatter.parseLocalTime(json.asString)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }
}
package com.bosleo.feedback.data.interview.contract

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.bosleo.feedback.api.InterviewRS
import com.bosleo.feedback.data.candidate.datasource.local.CandidateEntity
import com.bosleo.feedback.data.candidate.datasource.local.CandidateLocalSource
import com.bosleo.feedback.data.core.BaseDataRepo
import com.bosleo.feedback.data.interview.datasource.local.InterviewEntity
import com.bosleo.feedback.data.interview.datasource.local.InterviewLocalSource
import com.bosleo.feedback.data.interview.datasource.remote.InterviewRemoteSource
import com.bosleo.feedback.model.Candidate
import com.bosleo.feedback.model.Department
import com.bosleo.feedback.model.Interview
import com.bosleo.feedback.utility.Either
import com.bosleo.feedback.utility.generateIdIfBlank
import kotlinx.coroutines.rx2.await

class InterviewDataRepo(
    private val candidateLocalSource: CandidateLocalSource,
    private val interviewLocalSource: InterviewLocalSource,
    private val interviewRemoteSource: InterviewRemoteSource
) : BaseDataRepo(), InterviewRepo {

    override suspend fun getAllInterviews(): Either<Exception, LiveData<List<Interview>>> {
        return either({
            Transformations.map(interviewLocalSource.getAllInterviews()) { list ->
                list.filter { it.candidateWithInterview.isNotEmpty() }
                    .map { candidateInterviewJoin ->
                        val candidateEntity = candidateInterviewJoin.candidateEntity
                        val departmentEntity = candidateInterviewJoin.candidateWithDepartment.first()
                        candidateInterviewJoin.candidateWithInterview.first().toInterview().also {
                            it.candidate = candidateEntity.toCandidate().also {
                                it.department = Department(departmentEntity.id, departmentEntity.name)
                            }
                        }
                    }
            }
        }, {
            interviewRemoteSource.fetchAllInterviews().await().map {
                with(it.candidate) {
                    if (candidateLocalSource.addCandidate(toCandidateEntity()) > 0) {
                        interviewLocalSource.addInterview(it.toInterviewEntity())
                    }
                }
            }
        })
    }

    override suspend fun insertInterView(interview: Interview): Either<Exception, Boolean> = either {
        interview.id = interview.id.generateIdIfBlank()
        interviewRemoteSource.postInterviews(interview.toInterviewPRQ()).await()
        interviewLocalSource.addInterview(interview.toInterviewEntity())
        true
    }

    override suspend fun getInterviewByID(interviewId: String): Either<Exception, LiveData<Interview>> {
        return either {
            Transformations.map(
                interviewLocalSource.getInterviewById(interviewId)
            ) {
                it.toInterview()
            }
        }
    }

    override suspend fun updateInterview(interview: Interview): Either<Exception, Interview> {
        return either {
            interviewRemoteSource.putInterview(interview.toInterviewPRQ())
                .map {
                    val entity = it.toInterviewEntity()
                    interviewLocalSource.updateInterview(entity)
                    entity.toInterview()
                }
                .await()
        }
    }
}
package com.bosleo.feedback.data.interview.datasource.remote

import com.bosleo.feedback.api.InterviewPRQ
import com.bosleo.feedback.api.InterviewRS
import com.bosleo.feedback.api.StatusRS
import com.bosleo.feedback.model.Interview
import io.reactivex.Single

class InterviewRemoteDS(private val interviewApiService: InterviewApiService) : InterviewRemoteSource {
    override fun putInterview(interview: InterviewPRQ) = interviewApiService.putInterview(interview)

    override fun postInterviews(interview: InterviewPRQ) = interviewApiService.postInterview(interview)

    override fun fetchAllInterviews(): Single<List<InterviewRS>> {
        return interviewApiService.fetchAllInterviews()
    }

}
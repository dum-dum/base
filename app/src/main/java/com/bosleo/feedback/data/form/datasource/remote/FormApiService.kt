package com.bosleo.feedback.data.form.datasource.remote

import com.bosleo.feedback.api.AddFormPRQ
import com.bosleo.feedback.api.FormRS
import com.bosleo.feedback.api.StatusRS
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface FormApiService {

    @GET("form")
    fun fetchForms(): Single<List<FormRS>>

    @POST("form")
    fun addForms(@Body addFormPRQ: AddFormPRQ): Single<FormRS>

    @GET("form/{id}")
    fun getFormById(@Path("id") formId: String): Single<FormRS>
}
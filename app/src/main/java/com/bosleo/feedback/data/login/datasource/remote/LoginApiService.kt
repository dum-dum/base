package com.bosleo.feedback.data.login.datasource.remote

import com.bosleo.feedback.api.LoginPRQ
import com.bosleo.feedback.api.UserRs
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApiService {

    @POST("users/login")
    fun login(@Body loginPRQ: LoginPRQ): Single<UserRs>
}
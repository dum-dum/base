package com.bosleo.feedback.data.subskill.contract

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.bosleo.feedback.data.core.BaseDataRepo
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillEntity
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillLocalSource
import com.bosleo.feedback.data.subskill.datasource.remote.SubSkillRemoteSource
import com.bosleo.feedback.model.SubSkill
import com.bosleo.feedback.utility.Either
import com.bosleo.feedback.utility.toInt
import kotlinx.coroutines.rx2.await

class SubSkillDataRepo(
    private val subSkillLocalSource: SubSkillLocalSource,
    private val subSkillRemoteSource: SubSkillRemoteSource
) : BaseDataRepo(), SubSkillRepo {

    override suspend fun getAllSubSkills(): Either<Exception, LiveData<List<SubSkill>>> {
        return either({
            Transformations.map(subSkillLocalSource.getAllSubSkills()) {
                it.map {
                    SubSkill(it.id, it.name, it.marks, it.skillId)
                }
            }
        }, {
            subSkillRemoteSource.fetchAllSubSkills().await().map {
                subSkillLocalSource.addSubSkill(with(it) {
                    SubSkillEntity(id, name, marks, skillId,(!this.deletedAt.isNullOrBlank()).toInt())
                })
            }
        })
    }

    override suspend fun addSubSkill(subSkill: SubSkill): Either<Exception, SubSkill> {
        return either {
            subSkillRemoteSource.addSubSkill(subSkill.toSubSkillPRQ())
                .map {

                    val subSkillEntities = it.toSubSkillEntity()
                    subSkillLocalSource.addSubSkill(subSkillEntities)
                    subSkillEntities.toSubSkill()
                }.await()
        }
    }

    override suspend fun deleteSubSkill(skillId: String, subSkillIds: List<String>): Either<Exception, Boolean> {
        return either {
            subSkillRemoteSource.deleteSubSkill(skillId,subSkillIds)
                .map {
                    if(it.get("status").asBoolean) {
                        subSkillLocalSource.deleteSubSkills(subSkillIds)

                    }else{
                        false
                    }
                }.await()
        }
    }
}
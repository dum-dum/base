package com.bosleo.feedback.data.dbjoin

import androidx.room.Embedded
import androidx.room.Relation
import com.bosleo.feedback.data.candidate.datasource.local.CandidateEntity
import com.bosleo.feedback.data.core.database.TableContract
import com.bosleo.feedback.data.department.datasource.local.DepartmentEntity
import com.bosleo.feedback.data.form.datasource.local.FormEntity
import com.bosleo.feedback.data.interview.datasource.local.InterviewEntity
import com.bosleo.feedback.data.skill.datasource.local.SkillEntity
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillEntity
import com.bosleo.feedback.model.Skill
import com.bosleo.feedback.model.SkillWithSubSkill
import com.bosleo.feedback.model.SubSkill

data class CandidateInterviewJoin(@Embedded var candidateEntity: CandidateEntity) {
    @Relation(
        entity = InterviewEntity::class,
        parentColumn = TableContract.Candidate.ID,
        entityColumn = TableContract.Interview.CANDIDATE_ID
    )
    var candidateWithInterview: List<InterviewEntity> = arrayListOf()

    @Relation(
        entityColumn = TableContract.Department.ID, parentColumn = TableContract.Candidate.DEPARTMENT_ID
    )
    var candidateWithDepartment: List<DepartmentEntity> = arrayListOf()
}

data class DepartmentFormJoin(@Embedded var departmentEntity: DepartmentEntity) {
    @Relation(
        entity = FormEntity::class,
        parentColumn = TableContract.Department.ID,
        entityColumn = TableContract.Candidate.DEPARTMENT_ID
    )
    var departmentWithForm: List<FormSkillJoin> = arrayListOf()
}

data class FormSkillJoin(@Embedded var formEntity: FormEntity) {
    @Relation(
        entity = SkillEntity::class,
        entityColumn = TableContract.Skill.FORM_ID,
        parentColumn = TableContract.Form.ID
    )
    var formWithSkill: List<SkillSubSkillJoin> = arrayListOf()
}

data class SkillSubSkillJoin(@Embedded var skillEntity: SkillEntity) {
    @Relation(
        entityColumn = TableContract.SubSkill.SKILL_ID,
        parentColumn = TableContract.Skill.ID
    )
    var skillWithSubSkill: List<SubSkillEntity> = arrayListOf()

    fun toSkillWithSubSkill() = SkillWithSubSkill(
        Skill(skillEntity.id, skillEntity.name, skillEntity.formId),
        skillWithSubSkill.filter {
            it.isDeleted == 0
        }.map {
            SubSkill(it.id, it.name, it.marks, it.skillId)
        }
    )

}
package com.bosleo.feedback.data.core

import com.bosleo.feedback.utility.Constants
import com.google.gson.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.lang.reflect.Type

class DateTimeTypeConverter : JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

    override fun serialize(src: DateTime, srcType: Type, context: JsonSerializationContext): JsonElement {
        return JsonPrimitive(src.toString(Constants.FORMAT_TIMESTAMP))
    }

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, type: Type, context: JsonDeserializationContext): DateTime? {

        // Do not try to deserialize null or empty values
        if (json.asString == null || json.asString.isEmpty()) {
            return null
        }

        try {
            val formatter = DateTimeFormat.forPattern("YYYY-MM-dd")
            return formatter.parseDateTime(json.asString)
        } catch (e: Exception) {
        }

        try {
            val formatter = DateTimeFormat.forPattern(Constants.FORMAT_TIMESTAMP)
            return formatter.parseDateTime(json.asString)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val milliseconds = json.asLong * 1000L
        return DateTime(milliseconds)
    }
}
package com.bosleo.feedback.data.login.contract

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.bosleo.feedback.data.core.BaseDataRepo
import com.bosleo.feedback.data.login.datasource.local.LoginLocalSource
import com.bosleo.feedback.data.login.datasource.remote.LoginRemoteSource
import com.bosleo.feedback.model.User
import com.bosleo.feedback.utility.Constants
import com.bosleo.feedback.utility.Either
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.coroutines.rx2.await

class LoginDataRepo constructor(
    private val loginRemoteSource: LoginRemoteSource,
    private val loginLocalSource: LoginLocalSource
) :
    BaseDataRepo(), LoginRepo {


    override suspend fun doLogin(email: String, password: String): Either<Exception, Boolean> {
        return either {
            with(loginRemoteSource.doLogin(email, password).await()) {
                Prefs.putString(Constants.PrefKey.TOKEN, token)
                loginLocalSource.insertUser(toUserEntity())
                true
            }
        }
    }


    override suspend fun getUser(): Either<Exception, LiveData<User?>> {
        return either {
            val userLiveData = loginLocalSource.getUser()
            Transformations.map(userLiveData) {
                it?.toUser()
            }
        }
    }


}
package com.bosleo.feedback.data.department.datasource.remote

import com.bosleo.feedback.api.AddDepartmentPRQ
import com.bosleo.feedback.api.DepartmentRS
import com.bosleo.feedback.api.StatusRS
import io.reactivex.Single

interface DepartmentRemoteSource {

    fun fetchAllDepartments(): Single<List<DepartmentRS>>

    fun insertDepartment(addDepartmentPRQ: AddDepartmentPRQ):Single<DepartmentRS>
}
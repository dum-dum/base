package com.bosleo.feedback.data.form.datasource.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bosleo.feedback.data.core.database.TableContract
import com.bosleo.feedback.model.Form

@Entity(tableName = TableContract.Form.FORM)
data class FormEntity(
    @PrimaryKey
    @ColumnInfo(name = TableContract.Form.ID)
    var id: String,
    @ColumnInfo(name = TableContract.Form.DEPARTMENT_ID)
    var departmentId: String
){
    fun  toForm() = Form(id,departmentId)
}
package com.bosleo.feedback.data.interview.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.dbjoin.CandidateInterviewJoin

class InterviewLocalDS(private val interviewDao: InterviewDao) : InterviewLocalSource {

    override fun getAllInterviews(): LiveData<List<CandidateInterviewJoin>> {
        return interviewDao.getAllInterviews()
    }

    override fun addInterview(interviewEntity: InterviewEntity): Long {
        return interviewDao.insert(interviewEntity)
    }

    override fun getInterviewById(interviewId: String): LiveData<InterviewEntity> {
        return interviewDao.getInterviewById(interviewId)
    }

    override fun updateInterview(interviewEntity: InterviewEntity): Int {
        return interviewDao.update(interviewEntity)
    }
}
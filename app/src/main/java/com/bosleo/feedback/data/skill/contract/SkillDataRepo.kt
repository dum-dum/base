package com.bosleo.feedback.data.skill.contract

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.bosleo.feedback.api.SkillPRQ
import com.bosleo.feedback.data.core.BaseDataRepo
import com.bosleo.feedback.data.skill.datasource.local.SkillEntity
import com.bosleo.feedback.data.skill.datasource.local.SkillLocalSource
import com.bosleo.feedback.data.skill.datasource.remote.SkillRemoteSource
import com.bosleo.feedback.model.Skill
import com.bosleo.feedback.utility.Either
import com.bosleo.feedback.utility.toInt
import kotlinx.coroutines.rx2.await

class SkillDataRepo(
    private val skillLocalSource: SkillLocalSource,
    private val skillRemoteSource: SkillRemoteSource
) : BaseDataRepo(), SkillRepo {

    override suspend fun getAllSkills(): Either<Exception, LiveData<List<Skill>>> {
        return either({
            Transformations.map(skillLocalSource.getAllSkills()) {
                it.map {
                    Skill(it.id, it.name, it.formId)
                }
            }
        }, {
            skillRemoteSource.fetchAllSkills().await().map {
                skillLocalSource.addSkill(with(it) {
                    SkillEntity(id, name, formId, (!this.deletedAt.isNullOrBlank()).toInt())
                })
            }
        })
    }

    override suspend fun addSkill(skill: Skill): Either<Exception, Skill> {
        return either {
            skillRemoteSource.addSkill(SkillPRQ(skill.formId, skill.name)).map {
                it.toSkillEntity().skillEntity.also {
                    skillLocalSource.addSkill(it)
                }.toSkill()
            }.await()

        }
    }

    override suspend fun deleteSkill(skillId: String): Either<Exception, Boolean> {
        return either {
            skillRemoteSource.deleteSkill(skillId)
                .map {
                    if (it.get("status").asBoolean)
                        skillLocalSource.deleteSkill(skillId)
                    else
                        false
                }.await()
        }
    }

}
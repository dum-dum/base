package com.bosleo.feedback.data.form.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.dbjoin.FormSkillJoin

class FormLocalDS(private val formDao: FormDao) : FormLocalSource {

    override fun addForm(formEntity: List<FormEntity>) = formDao.insert(formEntity)

    override fun getForm(departmentId: String): FormEntity {
        return formDao.getFormByDepartmentId(departmentId)
    }

    override fun addForm(formEntity: FormEntity): Long {
        return formDao.insert(formEntity)
    }

    override fun getFormWithSkillById(formId: String) = formDao.getFomWithSkills(formId)
}
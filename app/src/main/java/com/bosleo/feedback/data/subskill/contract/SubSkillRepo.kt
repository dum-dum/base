package com.bosleo.feedback.data.subskill.contract

import androidx.lifecycle.LiveData
import com.bosleo.feedback.model.SubSkill
import com.bosleo.feedback.utility.Either

interface SubSkillRepo {

    suspend fun getAllSubSkills(): Either<Exception, LiveData<List<SubSkill>>>
    suspend fun addSubSkill(subSkill: SubSkill): Either<Exception, SubSkill>
    suspend fun deleteSubSkill(skillId: String, subSkillIds: List<String>): Either<Exception,Boolean>

}
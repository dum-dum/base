package com.bosleo.feedback.data.syncmaster

import com.bosleo.feedback.data.core.BaseDataRepo
import com.bosleo.feedback.data.department.datasource.local.DepartmentEntity
import com.bosleo.feedback.data.department.datasource.local.DepartmentLocalSource
import com.bosleo.feedback.data.department.datasource.remote.DepartmentRemoteSource
import com.bosleo.feedback.data.form.datasource.local.FormEntity
import com.bosleo.feedback.data.form.datasource.local.FormLocalSource
import com.bosleo.feedback.data.form.datasource.remote.FormRemoteSource
import com.bosleo.feedback.data.skill.datasource.local.SkillEntity
import com.bosleo.feedback.data.skill.datasource.local.SkillLocalSource
import com.bosleo.feedback.data.skill.datasource.remote.SkillRemoteSource
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillEntity
import com.bosleo.feedback.data.subskill.datasource.local.SubSkillLocalSource
import com.bosleo.feedback.utility.toInt
import io.reactivex.Single

class MasterDataRepo(
    private val departmentLocalSource: DepartmentLocalSource,
    private val departmentRemoteSource: DepartmentRemoteSource,
    private val formLocalSource: FormLocalSource,
    private val formRemoteSource: FormRemoteSource,
    private val skillLocalSource: SkillLocalSource,
    private val skillRemoteSource: SkillRemoteSource,
    private val subSkillLocalSource: SubSkillLocalSource
) : BaseDataRepo(), MasterRepo {

    override fun fetchMasters(): Single<List<Long>> {
        return departmentRemoteSource.fetchAllDepartments().map {
            it.map {
                departmentLocalSource.addDepartment(DepartmentEntity(it.id, it.name))
            }
        }.flatMap {
            formRemoteSource.fetchForm()
        }.map {
            it.map {
                formLocalSource.addForm(FormEntity(it.id, it.departmentId))
            }
        }.flatMap {
            skillRemoteSource.fetchAllSkills()
        }.map {
            it.map {
                skillLocalSource.addSkill(SkillEntity(it.id, it.name, it.formId,(!it.deletedAt.isNullOrBlank()).toInt())).apply {
                    if (this > 0) {
                        subSkillLocalSource.addSubSkills(it.subSkills.map {
                            SubSkillEntity(
                                it.id,
                                it.name,
                                it.marks,
                                it.skillId,
                                (!it.deletedAt.isNullOrBlank()).toInt()
                            )
                        })
                    }
                }
            }
        }
    }

}
package com.bosleo.feedback.data.skill.datasource.remote

import com.bosleo.feedback.api.SkillPRQ

class SkillRemoteDS(private val skillApiService: SkillApiService) : SkillRemoteSource {

    override fun fetchAllSkills() = skillApiService.fetchAllSkills()

    override fun addSkill(skillPRQ: SkillPRQ) = skillApiService.addSkill(skillPRQ)

    override fun deleteSkill(skillId: String) = skillApiService.deleteSkill(skillId)
}
package com.bosleo.feedback.data.sample

import com.bosleo.feedback.api.Post
import com.bosleo.feedback.utility.Either

interface SampleRepo {

    suspend fun fetchSomethingRx(): Either<Exception, List<Post>>

    suspend fun fetchSomethingCoroutine(): Either<Exception, List<Post>>

}
package com.bosleo.feedback.data.subskill.datasource.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bosleo.feedback.data.core.database.TableContract
import com.bosleo.feedback.model.SubSkill

@Entity(tableName = TableContract.SubSkill.SUB_SKILL)
data class SubSkillEntity(

    @PrimaryKey
    @ColumnInfo(name = TableContract.SubSkill.ID)
    var id: String,
    @ColumnInfo(name = TableContract.SubSkill.NAME)
    var name: String,
    @ColumnInfo(name = TableContract.SubSkill.MARKS)
    var marks: Int,
    @ColumnInfo(name = TableContract.SubSkill.SKILL_ID)
    var skillId: String,
    @ColumnInfo(name=TableContract.SubSkill.IS_DELETED)
    var isDeleted:Int
) {
    fun toSubSkill() = SubSkill(id, name, marks, skillId)
}
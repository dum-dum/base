package com.bosleo.feedback.data.form.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.bosleo.feedback.data.core.BaseDao
import com.bosleo.feedback.data.dbjoin.FormSkillJoin

@Dao
interface FormDao : BaseDao<FormEntity> {
    @Query("SELECT * FROM `form` WHERE `DEPARTMENT_ID`=:departmentID")
    fun getFormByDepartmentId(departmentID: String): FormEntity

    @Query("SELECT * FROM `form` where `ID`=:formId")
    fun getFomWithSkills(formId: String): LiveData<FormSkillJoin>
}
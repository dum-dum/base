package com.bosleo.feedback.data.login.datasource.remote

import com.bosleo.feedback.api.UserRs
import io.reactivex.Single

interface LoginRemoteSource {
    fun doLogin(email: String, password: String): Single<UserRs>
}
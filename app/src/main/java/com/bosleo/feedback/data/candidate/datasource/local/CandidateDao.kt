package com.bosleo.feedback.data.candidate.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.bosleo.feedback.data.core.BaseDao
import com.bosleo.feedback.data.dbjoin.CandidateInterviewJoin

@Dao
interface CandidateDao : BaseDao<CandidateEntity> {
    @Transaction
    @Query("SELECT * FROM `CANDIDATE`")
    fun getAllCandidate(): LiveData<List<CandidateInterviewJoin>>


    @Transaction
    @Query("SELECT * FROM `CANDIDATE` WHERE ID = :candidateId")
    fun getCandidateById(candidateId: String): CandidateInterviewJoin

}
package com.bosleo.feedback.data.form.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.dbjoin.FormSkillJoin

interface FormLocalSource {

    fun addForm(formEntity: FormEntity): Long
    fun addForm(formEntity: List<FormEntity>): List<Long>
    fun getForm(departmentId: String): FormEntity
    fun getFormWithSkillById(formId: String): LiveData<FormSkillJoin>
}
package com.bosleo.feedback.data.candidate.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.dbjoin.CandidateInterviewJoin

interface CandidateLocalSource {

    fun getAllCandidate(): LiveData<List<CandidateInterviewJoin>>

    fun addCandidate(candidateEntity: CandidateEntity): Long

    fun getCandidateById(candidateId:String):CandidateInterviewJoin
}
package com.bosleo.feedback.data.subskill.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.bosleo.feedback.data.core.BaseDao
import com.bosleo.feedback.data.core.database.TableContract

@Dao
interface SubSkillDao : BaseDao<SubSkillEntity> {

    @Query("SELECT * FROM ${TableContract.SubSkill.SUB_SKILL}")
    fun getAllSubSkills(): LiveData<List<SubSkillEntity>>

    @Query("UPDATE ${TableContract.SubSkill.SUB_SKILL} SET ${TableContract.SubSkill.IS_DELETED}=1 WHERE ${TableContract.SubSkill.ID} IN (:subSkillIds)")
    fun deleteSubSkills(subSkillIds: List<String>): Int

}
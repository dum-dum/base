package com.bosleo.feedback.data.skill.datasource.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bosleo.feedback.data.core.database.TableContract
import com.bosleo.feedback.model.Skill

@Entity(tableName = TableContract.Skill.SKILL)
data class SkillEntity(
    @PrimaryKey
    @ColumnInfo(name = TableContract.Skill.ID)
    var id: String,
    @ColumnInfo(name = TableContract.Skill.NAME)
    var name: String,
    @ColumnInfo(name = TableContract.Skill.FORM_ID)
    var formId: String,
    @ColumnInfo(name = TableContract.Skill.IS_DELETED)
    var isDeleted: Int
) {
    fun toSkill() = Skill(id,name,formId)
}
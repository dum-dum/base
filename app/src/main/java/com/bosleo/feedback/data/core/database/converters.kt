package com.bosleo.feedback.data.core.database

import androidx.room.TypeConverter
import org.joda.time.DateTime

class DateToLongConverter() {
    @TypeConverter
    fun getDateFromLong(long: Long): DateTime = DateTime(long)

    @TypeConverter
    fun getLongFromDate(dateTime: DateTime): Long = dateTime.millis

}
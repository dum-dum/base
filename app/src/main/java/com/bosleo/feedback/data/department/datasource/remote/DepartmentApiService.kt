package com.bosleo.feedback.data.department.datasource.remote

import com.bosleo.feedback.api.AddDepartmentPRQ
import com.bosleo.feedback.api.DepartmentRS
import com.bosleo.feedback.api.StatusRS
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface DepartmentApiService {

    @GET("department")
    fun getAllDepartments(): Single<List<DepartmentRS>>

    @POST("department")
    fun insertDepartment(@Body departmentPRQ: AddDepartmentPRQ):Single<DepartmentRS>
}
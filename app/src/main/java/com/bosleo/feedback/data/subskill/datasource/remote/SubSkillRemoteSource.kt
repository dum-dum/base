package com.bosleo.feedback.data.subskill.datasource.remote

import com.bosleo.feedback.api.SubSkillPRQ
import com.bosleo.feedback.api.SubSkillRS
import com.google.gson.JsonObject
import io.reactivex.Single

interface SubSkillRemoteSource {

    fun fetchAllSubSkills(): Single<List<SubSkillRS>>
    fun addSubSkill(subSkillPRQ: SubSkillPRQ):Single<SubSkillRS>
    fun deleteSubSkill(skillId: String, subSkillIds: List<String>): Single<JsonObject>

}
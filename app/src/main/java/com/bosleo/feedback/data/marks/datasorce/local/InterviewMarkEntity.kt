package com.bosleo.feedback.data.marks.datasorce.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bosleo.feedback.data.core.database.TableContract
import com.bosleo.feedback.model.InterviewMark

@Entity(tableName = TableContract.InterviewMark.INTERVIEW_MARK)
data class InterviewMarkEntity(
    @PrimaryKey
    @ColumnInfo(name = TableContract.InterviewMark.ID)
    var id: String,
    @ColumnInfo(name = TableContract.InterviewMark.SKILL_ID)
    var skillId: String,
    @ColumnInfo(name = TableContract.InterviewMark.INTERVIEW_ID)
    var interviewId: String,
    @ColumnInfo(name = TableContract.InterviewMark.SUB_SKILL_ID)
    var subSkillId: String,
    @ColumnInfo(name = TableContract.InterviewMark.MARK)
    var mark: Int
) {
    fun toInterviewMarks() = InterviewMark(id, skillId, interviewId, subSkillId, mark)
}
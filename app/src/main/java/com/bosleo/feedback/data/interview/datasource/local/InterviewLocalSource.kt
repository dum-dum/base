package com.bosleo.feedback.data.interview.datasource.local

import androidx.lifecycle.LiveData
import com.bosleo.feedback.data.dbjoin.CandidateInterviewJoin

interface InterviewLocalSource {

    fun getAllInterviews(): LiveData<List<CandidateInterviewJoin>>

    fun addInterview(interviewEntity: InterviewEntity): Long

    fun getInterviewById(interviewId: String): LiveData<InterviewEntity>

    fun updateInterview(interviewEntity: InterviewEntity):Int
}
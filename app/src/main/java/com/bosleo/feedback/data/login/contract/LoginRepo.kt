package com.bosleo.feedback.data.login.contract

import androidx.lifecycle.LiveData
import com.bosleo.feedback.model.User
import com.bosleo.feedback.utility.Either

interface LoginRepo {

    suspend fun doLogin(email: String, password: String): Either<Exception, Boolean>

    suspend fun getUser(): Either<Exception, LiveData<User?>>

}
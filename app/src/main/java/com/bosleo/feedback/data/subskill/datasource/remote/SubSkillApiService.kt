package com.bosleo.feedback.data.subskill.datasource.remote

import com.bosleo.feedback.api.SubSkillDeletePRQ
import com.bosleo.feedback.api.SubSkillPRQ
import com.bosleo.feedback.api.SubSkillRS
import com.google.gson.JsonObject
import io.reactivex.Single
import retrofit2.http.*

interface SubSkillApiService {

    @GET("sub-skill")
    fun fetchAllSubSkills(): Single<List<SubSkillRS>>

    @POST("skill/{skillID}/sub_skill")
    fun addSubSkill(@Path("skillID") skillID: String, @Body subSkillPRQ: SubSkillPRQ): Single<SubSkillRS>

    @HTTP(method = "DELETE",path = "skill/{skill_id}/sub_skill",hasBody = true)
    fun deleteSubSkillId(@Path("skill_id")skillId: String, @Body subSkillDeletePRQ: SubSkillDeletePRQ): Single<JsonObject>
}
package com.bosleo.feedback.data.marks.contract

import com.bosleo.feedback.model.InterviewMark
import com.bosleo.feedback.utility.Either

interface InterviewMarksRepo {

    suspend fun addMark(interviewMark: InterviewMark): Either<Exception, InterviewMark>

    suspend fun addMark(interviewMarks: List<InterviewMark>): Either<Exception, List<InterviewMark>>

    suspend fun getMarksForInterView(interviewID: String): Either<Exception, List<InterviewMark>>

}
package com.bosleo.feedback.data.candidate.datasource.remote

import com.bosleo.feedback.api.CandidatePRQ
import com.bosleo.feedback.api.CandidateRS
import com.bosleo.feedback.api.StatusRS
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.reactivex.Single
import retrofit2.http.*

interface CandidateApiService {

    @FormUrlEncoded
    @POST("user/login")
    fun login(@Field("email") email: String, @Field("password") password: String): Single<JsonObject>

    @GET("interview-marks")
    fun getInterViewMark(): Single<JsonArray>


    @POST("candidate")
    fun insertCandidate(@Body candidatePRQ: CandidatePRQ): Single<CandidateRS>

    @GET("candidate")
    fun getAllCandidate(): Single<List<CandidateRS>>

}
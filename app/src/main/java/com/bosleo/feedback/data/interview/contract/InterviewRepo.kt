package com.bosleo.feedback.data.interview.contract

import androidx.lifecycle.LiveData
import com.bosleo.feedback.model.Interview
import com.bosleo.feedback.utility.Either

interface InterviewRepo {

    suspend fun getAllInterviews(): Either<Exception, LiveData<List<Interview>>>
    suspend fun insertInterView(interview: Interview): Either<Exception, Boolean>
    suspend fun getInterviewByID(interviewId: String): Either<Exception, LiveData<Interview>>
    suspend fun updateInterview(interview: Interview):Either<Exception,Interview>
}
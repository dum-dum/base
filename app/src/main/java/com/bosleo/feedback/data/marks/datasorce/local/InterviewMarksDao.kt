package com.bosleo.feedback.data.marks.datasorce.local

import androidx.room.Dao
import androidx.room.Query
import com.bosleo.feedback.data.core.BaseDao

@Dao
interface InterviewMarksDao : BaseDao<InterviewMarkEntity> {

    @Query("SELECT * FROM `INTERVIEW_MARK` WHERE INTERVIEW_ID = :interviewId")
    fun getInterviewMarks(interviewId: String): List<InterviewMarkEntity>

    @Query("SELECT * FROM `INTERVIEW_MARK` WHERE ID = :id")
    fun getInterviewMarkByID(id: String): InterviewMarkEntity

}
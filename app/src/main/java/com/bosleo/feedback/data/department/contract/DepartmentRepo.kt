package com.bosleo.feedback.data.department.contract

import androidx.lifecycle.LiveData
import com.bosleo.feedback.model.Department
import com.bosleo.feedback.model.DepartmentWithForm
import com.bosleo.feedback.utility.Either

interface DepartmentRepo {

    suspend fun getAllDepartments(): Either<Exception, LiveData<List<Department>>>

    suspend fun insertDepartment(department:Department):Either<Exception,Department>

    suspend fun getDepartmentWithForm(departmentId: String): Either<Exception, DepartmentWithForm>

}
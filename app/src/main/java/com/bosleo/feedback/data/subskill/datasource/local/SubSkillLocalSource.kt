package com.bosleo.feedback.data.subskill.datasource.local

import androidx.lifecycle.LiveData

interface SubSkillLocalSource {

    fun getAllSubSkills(): LiveData<List<SubSkillEntity>>

    fun addSubSkill(subSkillEntity: SubSkillEntity): Long

    fun addSubSkills(subSkillEntities: List<SubSkillEntity>): List<Long>

    fun deleteSubSkills(subSkillIds: List<String>):Boolean

}
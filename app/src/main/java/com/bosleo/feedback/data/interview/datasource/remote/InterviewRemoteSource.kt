package com.bosleo.feedback.data.interview.datasource.remote

import com.bosleo.feedback.api.InterviewPRQ
import com.bosleo.feedback.api.InterviewRS
import com.bosleo.feedback.api.StatusRS
import com.bosleo.feedback.model.Interview
import io.reactivex.Single

interface InterviewRemoteSource {

    fun fetchAllInterviews(): Single<List<InterviewRS>>
    fun postInterviews(interview: InterviewPRQ): Single<InterviewRS>
    fun putInterview(interview: InterviewPRQ):Single<InterviewRS>
}
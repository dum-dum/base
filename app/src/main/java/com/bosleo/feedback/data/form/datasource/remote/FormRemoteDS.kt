package com.bosleo.feedback.data.form.datasource.remote

import com.bosleo.feedback.api.AddFormPRQ
import com.bosleo.feedback.api.FormRS
import com.bosleo.feedback.api.StatusRS
import io.reactivex.Single

class FormRemoteDS(private val formApiService: FormApiService) : FormRemoteSource {

    override fun addForm(id: String, departmentId: String) = formApiService.addForms(AddFormPRQ(id, departmentId))

    override fun fetchForm(): Single<List<FormRS>> {
        return formApiService.fetchForms()
    }

    override fun getFormById(id: String): Single<FormRS> = formApiService.getFormById(id)
}
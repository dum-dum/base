package com.bosleo.feedback.data.skill.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.bosleo.feedback.data.core.BaseDao
import com.bosleo.feedback.data.core.database.TableContract

@Dao
interface SkillDao : BaseDao<SkillEntity> {

    @Query("SELECT * FROM ${TableContract.Skill.SKILL}")
    fun getAllSkills(): LiveData<List<SkillEntity>>

    @Query("UPDATE ${TableContract.Skill.SKILL} SET ${TableContract.Skill.IS_DELETED}=1 WHERE ${TableContract.Skill.ID}=:skillId")
    fun deleteSkill(skillId: String): Int

}
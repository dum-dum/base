package com.bosleo.feedback.data.marks.datasorce.remote

import com.bosleo.feedback.api.FeedbackPRQ

class InterviewMarkRemoteDS constructor(
    private val interviewMarksApiService: InterviewMarksApiService
) : InterviewMarkRemoteSource {

    override fun addUpdateFeedback(feedbackPRQ: FeedbackPRQ) =
        interviewMarksApiService.submitMarks(feedbackPRQ)

    override fun fetchFeedbackByInterviewID(interviewID: String) =
        interviewMarksApiService.fetchFeedbackByInterviewId(interviewID)
}
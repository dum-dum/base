package com.bosleo.feedback.utility

import android.R
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.transition.Fade
import android.transition.Slide
import android.transition.Transition
import android.transition.TransitionInflater
import android.util.DisplayMetrics
import android.view.*
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.github.abdularis.civ.AvatarImageView
import com.google.android.material.snackbar.Snackbar
import io.codetail.animation.ViewAnimationUtils
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.io.File
import java.math.RoundingMode
import java.util.*
import kotlin.random.Random

fun Window.disableTouch() {
    setFlags(
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
    )
}

fun Window.enableTouch() {
    clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
}

val <T> List<T>.lastIndex: Int
    get() = size - 1

fun <T> List<T>?.isNullOrEmpty(): Boolean {
    return this == null || this.isEmpty()
}

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

fun Activity.showKeyboard() {
    (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun Activity?.hideKeyboard() {
    if (this != null && !this.isFinishing) {
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    }
}

fun String.isValidPhoneNumber(): Boolean {
    if (this.length < 10) {
        return false
    }
    return android.util.Patterns.PHONE.matcher(this).matches()
}

fun String.isValidEmail(): Boolean {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun Activity.showSnackBar(
    msg: String,
    view: View = (this.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0),
    actionMsg: String? = "OK", clickAction: ((Snackbar) -> Unit)? = null
) {
    Snackbar.make(view, msg, Snackbar.LENGTH_LONG).apply {
        this.setAction(actionMsg) { _ ->
            clickAction?.invoke(this)
        }
    }.show()
}

fun Activity.showSnackBar(
    msg: String,
    view: View = (this.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0),
    actionMsg: String? = "OK", clickAction: ((Snackbar) -> Unit)? = null, callback: () -> Unit
) {
    Snackbar.make(view, msg, Snackbar.LENGTH_LONG).apply {
        this.setAction(actionMsg) { _ ->
            clickAction?.invoke(this)
        }
    }.addCallback(object : Snackbar.Callback() {
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            super.onDismissed(transientBottomBar, event)
            callback()
        }
    }).show()
}

fun Activity.showShortToast(
    msg: String
) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}


fun Activity.showDialog(
    msg: String, positiveListener: DialogInterface.OnClickListener? = null,
    negativeListener: DialogInterface.OnClickListener? = null,
    title: String = "Acuro", positive: String = "Yes", negative: String = "No"
) {

    val dialog = AlertDialog.Builder(this)
        .setMessage(msg)
    positiveListener?.let {
        dialog.setPositiveButton(positive, positiveListener)
    }

    negativeListener?.let {
        dialog.setNegativeButton(negative, negativeListener)
    }
    dialog.show()
}

fun Activity.getRealPathFromURI(contentUri: Uri?): String {
    val proj = arrayOf(MediaStore.Images.Media.DATA)
    val loader = androidx.loader.content.CursorLoader(this, contentUri!!, proj, null, null, null)
    val cursor = loader.loadInBackground()
    val result: String?
    try {
        val columnIndex = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        result = cursor.getString(columnIndex)
        cursor.close()
    } catch (ex: IllegalArgumentException) {
        return ""
    }

    return if (result != null && !result.isEmpty()) {
        result
    } else {
        ""
    }
}

fun View.setVisibility(isVisible: Boolean) {
    if (isVisible)
        this.visible()
    else
        this.gone()
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun <T> Collection<T>.getUnique(list: Collection<T>): Collection<T> {
    val union = this.union(list).toMutableList()
    val intersection = this.intersect(list)
    union.removeAll(intersection)
    return union
}

fun File.getMimeType(fallback: String = "image/*"): String {
    return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(toString()).toLowerCase())
        ?: fallback
}

fun String?.updateSyncStatusFromId(): String {
    return if (this.isNullOrBlank()) {
        "INSERT"
    } else {
        "UPDATE"
    }
}

fun Boolean.toInt(): Int {
    return if (this) {
        1
    } else {
        0
    }
}

fun Int.toBoolean(): Boolean {
    return this == 1
}

fun View.layoutToBitmap(): Bitmap {
    this.setDrawingCacheEnabled(true);

    this.measure(
        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
    );

    this.layout(0, 0, this.getMeasuredWidth(), this.getMeasuredHeight());

    this.buildDrawingCache(true);

    return Bitmap.createBitmap(this.getDrawingCache());

}

/**
 * Clears the list and adds all the elements from the newList
 **/
fun <T> MutableList<T>.replaceAll(newList: List<T>) {
    clear()
    addAll(newList)
}

fun Double.roundTo4decimal(): Double {
    return toBigDecimal().setScale(4, RoundingMode.UP).toDouble()
}

fun Activity.isAboveLollipop(): Boolean {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
}

fun String.toDateTime(fromPattern: String): DateTime {
    val formatter = DateTimeFormat.forPattern(fromPattern)
    return formatter.parseDateTime(this)
}

fun Context.isTablet(): Boolean {
    return this.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_LARGE

}

fun Context.convertDpToPixel(dp: Float): Float {
    val resources = this.resources
    val metrics = resources.displayMetrics
    return dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

fun Context.convertPixelsToDp(px: Float): Float {
    return px / (this.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

fun DateTime.withTimeAtEndOfDay(): DateTime {
    return this.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59)
}

fun String.toDateTime(): DateTime {
    val formatter = DateTimeFormat.forPattern("HH:mm")
    val time = formatter.parseDateTime(this)
    return DateTime().withHourOfDay(time.hourOfDay)
        .withMinuteOfHour(time.minuteOfHour)
        .withSecondOfMinute(0)
        .withMillisOfSecond(0)
}

fun <T> LiveData<T>.asMediatorLiveData(): MediatorLiveData<T> = this as MediatorLiveData<T>

inline fun View.waitForLayout(crossinline f: () -> Unit) = with(viewTreeObserver) {
    addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            this@waitForLayout.viewTreeObserver.removeOnGlobalLayoutListener(this)
            f()
        }
    })
}

fun String?.generateIdIfBlank(): String {
    return if (isNullOrBlank())
        UUID.randomUUID().toString()
    else {
        this.orEmpty()
    }
}


fun ViewGroup.revealActivity(revealX: Float, revealY: Float, revealColor: Int) {
//        val cx = (fabSave.getLeft() + fabSave.getRight()) / 2
//        val cy = (fabSave.getTop() + fabSave.getBottom()) / 2

    // get the final radius for the clipping circle
    val clRoot = this as ViewGroup
    val dx = Math.max(revealX, clRoot.width - revealX)
    val dy = Math.max(revealY, clRoot.height - revealY)
    val finalRadius = Math.hypot(dx.toDouble(), dy.toDouble()).toFloat()

    // Android native animator

    val animator =
        ViewAnimationUtils.createCircularReveal(clRoot, revealX.toInt(), revealY.toInt(), 28f, finalRadius)
    animator.interpolator = AccelerateDecelerateInterpolator()
    animator.duration = 700


    val colorTo = Color.TRANSPARENT
    clRoot.setBackgroundColor(revealColor)
    val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), revealColor, colorTo)
    colorAnimation.duration = 500 // milliseconds
    colorAnimation.addUpdateListener { anim -> clRoot.setBackgroundColor(anim.animatedValue as Int) }


    animator.addListener(object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {
        }

        override fun onAnimationEnd(animation: Animator?) {
        }

        override fun onAnimationCancel(animation: Animator?) {
        }

        override fun onAnimationStart(animation: Animator?) {
            colorAnimation.startDelay = 250
            colorAnimation.start()

        }

    })
    animator.start()

}

fun ViewGroup.hideActivity(
    revealX: Float,
    revealY: Float,
    revealColor: Int,
    animationListener: Animator.AnimatorListener
) {


    // get the final radius for the clipping circle
    val clRoot = this
    val dx = Math.max(revealX, clRoot.getWidth() - revealX)
    val dy = Math.max(revealY, clRoot.getHeight() - revealY)
    val finalRadius = Math.hypot(dx.toDouble(), dy.toDouble()).toFloat()

    // Android native animator

    val animator =
        ViewAnimationUtils.createCircularReveal(clRoot, revealX.toInt(), revealY.toInt(), finalRadius, 28f)
    animator.interpolator = AccelerateDecelerateInterpolator()
    animator.duration = 700


    val colorFrom = Color.TRANSPARENT
    val colorTo = revealColor
    val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
    colorAnimation.duration = 500 // milliseconds
    colorAnimation.addUpdateListener { anim -> clRoot.setBackgroundColor(anim.animatedValue as Int) }

    colorAnimation.addListener(animationListener)
    animator.start()
    colorAnimation.start()


}

fun AvatarImageView.setRandomBackground() {
    this.avatarBackgroundColor = Color.argb(
        255,
        255 - Random.nextInt(128),
        255 - Random.nextInt(128),
        255 - Random.nextInt(128)
    )

}

fun View.setRandomBackground() {
    this.setBackgroundColor(
        Color.argb(
            255,
            255 - Random.nextInt(128),
            255 - Random.nextInt(128),
            255 - Random.nextInt(128)
        )
    )

}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Activity.setupEnterAnimations(rootView: View, showView: View, vararg tragets: View) {
    val transition = TransitionInflater.from(this)
        .inflateTransition(com.bosleo.feedback.R.transition.changebounds_with_arcmotion)
    window.sharedElementEnterTransition = transition
    transition.addListener(object : Transition.TransitionListener {
        override fun onTransitionEnd(transition: Transition) {
            // Removing listener here is very important because shared element transition is executed again backwards on exit. If we don't remove the listener this code will be triggered again.
            transition.removeListener(this);

            tragets.forEach {
                it.gone()
            }
            animateRevealShow(rootView)
            animateRevealShow(showView)
//                animateButtonsIn();
        }

        override fun onTransitionResume(transition: Transition) {
        }

        override fun onTransitionPause(transition: Transition) {
        }

        override fun onTransitionCancel(transition: Transition) {
        }

        override fun onTransitionStart(transition: Transition) {
        }

    })
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Activity.animateRevealShow(viewRoot: View) {
    val cx = (viewRoot.getLeft() + viewRoot.getRight()) / 2
    val cy = (viewRoot.getTop() + viewRoot.getBottom()) / 2
    val finalRadius = Math.max(viewRoot.getWidth(), viewRoot.getHeight())

    val anim = android.view.ViewAnimationUtils.createCircularReveal(viewRoot, cx, cy, 0F, finalRadius.toFloat())
    viewRoot.setVisibility(View.VISIBLE)
    anim.setDuration(500L)
    anim.setInterpolator(AccelerateInterpolator())
    anim.start()
}


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Activity.setupExitAnimations(rootView: View) {
    val returnTransition = Fade()
    window.returnTransition = returnTransition
    returnTransition.duration = resources.getInteger(com.bosleo.feedback.R.integer.anim_duration_medium).toLong()
    returnTransition.startDelay = resources.getInteger(com.bosleo.feedback.R.integer.anim_duration_medium).toLong()
    returnTransition.addListener(object : Transition.TransitionListener {
        override fun onTransitionStart(transition: Transition) {
            transition.removeListener(this)
//                animateButtonsOut()
            animateRevealHide(rootView)
        }

        override fun onTransitionEnd(transition: Transition) {}

        override fun onTransitionCancel(transition: Transition) {}

        override fun onTransitionPause(transition: Transition) {}

        override fun onTransitionResume(transition: Transition) {}
    })
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Activity.animateRevealHide(viewRoot: View) {
    val cx = (viewRoot.left + viewRoot.right) / 2
    val cy = (viewRoot.top + viewRoot.bottom) / 2
    val initialRadius = viewRoot.width

    val anim = android.view.ViewAnimationUtils.createCircularReveal(viewRoot, cx, cy, initialRadius.toFloat(), 0f)
    anim.addListener(object : AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator) {
            super.onAnimationEnd(animation)
            viewRoot.visibility = View.INVISIBLE
        }
    })
    anim.duration = resources.getInteger(com.bosleo.feedback.R.integer.anim_duration_medium).toLong()
    anim.start()
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Activity.setupRevealWindowAnimations(rootView: View, showTargets: View, vararg hideTargets: View) {
//    interpolator = AnimationUtils.loadInterpolator(this, android.R.interpolator.linear_out_slow_in)
    setupEnterAnimations(rootView, showTargets, *hideTargets)
    setupExitAnimations(rootView)
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Activity.setupSlideWindowAnimations() {
    // Re-enter transition is executed when returning to this activity
    val slideTransition = Slide()
    slideTransition.setSlideEdge(Gravity.LEFT)
    slideTransition.setDuration(resources.getInteger(R.integer.config_mediumAnimTime).toLong())
    window.reenterTransition = slideTransition
    window.exitTransition = slideTransition
}

fun Int.manipulateColor(factor: Float): Int {
    val a = Color.alpha(this)
    val r = Math.round(Color.red(this) * factor)
    val g = Math.round(Color.green(this) * factor)
    val b = Math.round(Color.blue(this) * factor)
    return Color.argb(
        a,
        Math.min(r, 255),
        Math.min(g, 255),
        Math.min(b, 255)
    )
}
package com.bosleo.feedback.utility

object Constants {

    const val FORMAT_TIMESTAMP = "YYYY-MM-dd HH:mm:ss"
    const val FORMAT_DDMMYYYY = "MMM dd, yyyy"


    const val EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X "
    const val EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y"
    const val EXTRA_CIRCULAR_REVEAL_COLOR = "EXTRA_CIRCULAR_REVEAL_COLOR"

    const val EXTRA_ID = "ID"
    const val EXTRA_INTERVIEW_ID = "INTERVIEW_ID"
    const val EXTRA_NAME = "NAME"

    object PrefKey {
        const val TOKEN = "token"
    }

}
package com.bosleo.feedback.presentation.form

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.bosleo.feedback.data.form.contract.FormRepo
import com.bosleo.feedback.data.skill.contract.SkillRepo
import com.bosleo.feedback.data.subskill.contract.SubSkillRepo
import com.bosleo.feedback.model.Form
import com.bosleo.feedback.model.Skill
import com.bosleo.feedback.model.SubSkill
import com.bosleo.feedback.presentation.core.BaseViewModel
import com.bosleo.feedback.utility.asMediatorLiveData
import com.bosleo.feedback.utility.map
import org.koin.standalone.inject

class FormDetailViewModel : BaseViewModel() {

    val formRepo by inject<FormRepo>()
    val skillRepo by inject<SkillRepo>()
    val subSkillRepo by inject<SubSkillRepo>()
    val deleteSubSkillLiveData: MutableLiveData<List<String>> by lazy { MutableLiveData<List<String>>() }
    val deleteSkillLiveData: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    val formLiveData: MutableLiveData<Form> by lazy { MediatorLiveData<Form>() }

    val addSkillLiveData: MutableLiveData<Skill> by lazy { MediatorLiveData<Skill>() }
    val addSubSkillLiveData: MutableLiveData<SubSkill> by lazy { MediatorLiveData<SubSkill>() }

    fun fetchFormData(formId: String) {
        postLiveValue(formLiveData.asMediatorLiveData()) {
            formRepo.getFormWithSkills(formId)
        }

    }

    fun addSkill(name: String) {
        postValue(addSkillLiveData) {
            skillRepo.addSkill(Skill("", name, formLiveData.value?.id.orEmpty()))
        }
    }

    fun addSubSkill(name: String, skillId: String) {
        postValue(addSubSkillLiveData) {
            subSkillRepo.addSubSkill(SubSkill("", name, 10, skillId))
        }

    }

    fun deleteSubSkills(skillId: String, subSkillIds: List<String>) {
        postValue(deleteSubSkillLiveData) {
            subSkillRepo.deleteSubSkill(skillId, subSkillIds).map {
                if (it) {
                    subSkillIds
                } else {
                    emptyList()
                }
            }
        }
    }

    fun deleteSkill(skillId: String) {
        postValue(deleteSkillLiveData) {
            skillRepo.deleteSkill(skillId).map {
                if (it) {
                    skillId
                } else {
                    ""
                }
            }
        }
    }
}
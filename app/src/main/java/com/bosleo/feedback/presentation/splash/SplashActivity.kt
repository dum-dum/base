package com.bosleo.feedback.presentation.splash

import android.content.Intent
import android.os.Bundle
import com.bosleo.feedback.R
import com.bosleo.feedback.presentation.core.BaseActivity
import com.bosleo.feedback.presentation.home.HomeActivity
import com.bosleo.feedback.presentation.login.LoginActivity
import com.bosleo.feedback.utility.gone
import com.bosleo.feedback.utility.visible
import kotlinx.android.synthetic.main.activity_splash.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashActivity : BaseActivity<SplashViewModel>() {

    private val splashViewModel by viewModel<SplashViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        init()
    }

    override fun getViewModel() = splashViewModel

    private fun init() {
        splashViewModel.startSync()
    }

    override fun onProgressStart() {
        pbSyncing.visible()
    }

    override fun onProgressComplete() {
        pbSyncing.gone()
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

}

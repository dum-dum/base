package com.bosleo.feedback.presentation.form.adapter

import android.graphics.Color
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bosleo.feedback.R
import com.bosleo.feedback.model.SubSkill
import com.bosleo.feedback.utility.setRandomBackground
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.flexibleadapter.utils.DrawableUtils
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.cell_department.view.*
import kotlinx.android.synthetic.main.flip_frontview.view.*

class SkillAdapterItem(val subSkill: SubSkill) :
    AbstractFlexibleItem<SkillAdapterItem.SubSkillListViewHolder>() {
    private lateinit var onImgClick: (SubSkill,Boolean) -> Unit
    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: SubSkillListViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        holder?.itemView?.isActivated = adapter?.selectedPositions?.contains(position) ?: false
        val drawable = DrawableUtils.getSelectableBackgroundCompat(
            Color.parseColor("#FFFFFF"),             // normal background
     holder?.itemView?.context?.let { ContextCompat.getColor(it, android.R.color.darker_gray) }
                ?: Color.DKGRAY, // pressed background
            Color.WHITE
        ) // ripple color

        DrawableUtils.setBackgroundCompat(holder?.itemView, drawable)
        holder?.itemView?.apply {
            tvDepartment.text = subSkill.name
            flipView?.flip(holder.itemView.isActivated)
            ivAvatar.setOnClickListener {
                flipView.performClick()
            }
            ivAvatar.setRandomBackground()
            ivAvatar.setText(subSkill.name.first().toString().toUpperCase())
            flipView.setOnClickListener {
                if (::onImgClick.isInitialized) {
                    isActivated = !isActivated
                    flipView.flip(isActivated)
                    onImgClick(subSkill,flipView.isFlipped)
                }
            }
        }
    }

    override fun equals(other: Any?) = other is SkillAdapterItem && other.subSkill.id == subSkill.id

    override fun createViewHolder(
        view: View,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
    ) = SubSkillListViewHolder(view, adapter)


    override fun getLayoutRes() = R.layout.cell_department
    override fun hashCode(): Int {
        return subSkill.hashCode()
    }

    fun onImageClick(onImageClick: (SubSkill,Boolean) -> Unit) {
        this.onImgClick = onImageClick
    }


    inner class SubSkillListViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>) :
        FlexibleViewHolder(view, adapter)
}
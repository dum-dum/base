package com.bosleo.feedback.presentation.home

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import com.bosleo.feedback.R
import com.bosleo.feedback.presentation.candidate.CandidateActivity
import com.bosleo.feedback.presentation.core.BaseActivity
import com.bosleo.feedback.presentation.form.FormListActivity
import com.bosleo.feedback.presentation.interview.InterviewActivity
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_COLOR
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_X
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_Y
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeActivity : BaseActivity<HomeViewModel>() {

    private val homeViewModel by viewModel<HomeViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        init()
    }

    override fun getViewModel() = homeViewModel

    private fun init() {
        setSupportActionBar(toolbar)
        clInterview.setOnClickListener {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, avatarImageView1, "transitionCandidate")
            val rect = Rect()
            avatarImageView1.getGlobalVisibleRect(rect)

            val revealX: Float = (rect.left + avatarImageView1.width / 2).toFloat()
            val revealY: Float = (rect.top + avatarImageView1.height / 2).toFloat()

            val intent = Intent(this, InterviewActivity::class.java)
            intent.putExtra(EXTRA_CIRCULAR_REVEAL_X, revealX)
            intent.putExtra(EXTRA_CIRCULAR_REVEAL_Y, revealY)
            intent.putExtra(EXTRA_CIRCULAR_REVEAL_COLOR, cv01.cardBackgroundColor.defaultColor)

            ActivityCompat.startActivity(this, intent, options.toBundle())
        }
        clForm.setOnClickListener {
            startFormActivity()
        }
        clCandidate.setOnClickListener {
            startAddCandidateActivity()
        }
    }

    private fun startFormActivity(){
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, avatarImageView3, "transitionCandidate")
        val rect = Rect()
        avatarImageView3.getGlobalVisibleRect(rect)

        val revealX: Float = (rect.left + avatarImageView3.width / 2).toFloat()
        val revealY: Float = (rect.top + avatarImageView3.height / 2).toFloat()

        val intent = Intent(this, FormListActivity::class.java)
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_X, revealX)
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_Y, revealY)
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_COLOR, cv03.cardBackgroundColor.defaultColor)

        ActivityCompat.startActivity(this, intent, options.toBundle())
    }

    private fun startAddCandidateActivity() {

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, avatarImageView2, "transitionCandidate")
        val rect = Rect()
        avatarImageView2.getGlobalVisibleRect(rect)

        val revealX: Float = (rect.left + avatarImageView2.width / 2).toFloat()
        val revealY: Float = (rect.top + avatarImageView2.height / 2).toFloat()

        val intent = Intent(this, CandidateActivity::class.java)
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_X, revealX)
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_Y, revealY)
        intent.putExtra(EXTRA_CIRCULAR_REVEAL_COLOR, cv02.cardBackgroundColor.defaultColor)

        ActivityCompat.startActivity(this, intent, options.toBundle())
    }

}

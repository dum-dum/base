package com.bosleo.feedback.presentation.core

import android.view.MenuItem
import android.view.View
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bosleo.feedback.R
import com.bosleo.feedback.utility.*
import com.orhanobut.logger.Logger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BaseActivity<out V : BaseViewModel> : AppCompatActivity(), CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job

    private var autoErrorHandle: Boolean = false

    abstract fun getViewModel(): V

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    protected open fun getLoadingView(): View? {
        return null
    }

    protected open fun onProgressStart() {
        getLoadingView()?.visible()
    }


    protected open fun onProgressComplete() {
        getLoadingView()?.gone()
    }

    override fun onResume() {
        super.onResume()

        attachErrorObserver()
        attachLoadingObserver()
    }

    override fun onDestroy() {
        job.cancel()
        super.onDestroy()
    }

    private fun attachLoadingObserver() {
        getViewModel().loadingLiveData.observe(this, Observer {
            it?.apply {
                if (this) {
                    onProgressStart()
                } else {
                    onProgressComplete()
                }
            } ?: onProgressComplete()
        })
    }

    fun attachErrorObserver() {
        getViewModel().exceptionLiveData.observe(this, Observer {
            hideProgress()
            it?.apply {
                val type = handleError(it)
                val e: Any = when (type) {
                    is ExceptionHandleType.SnackBarError -> {
                        Logger.e(type.throwable, "snack bar error: ${type.message}")

                        showSnackBar(type.message)
                    }
                    is ExceptionHandleType.LogOnlyError -> {
                        type.throwable.printStackTrace()
                        Logger.e(type.throwable, "unknown error: ${type.message}")
                    }
                    is ExceptionHandleType.ToastError -> {
                        Logger.e(type.throwable, "toast error: ${type.message}")
                        showShortToast(type.message)
                    }
                    is ExceptionHandleType.DialogError -> {
                        Logger.e(type.throwable, "dialog error: ${type.message}")
                        showDialog(type.message)
                    }
                    is ExceptionHandleType.DialogWithConfirmError -> TODO()
                }
            }
        })
    }

    private fun handleError(exception: Exception): BaseActivity.ExceptionHandleType {
        return BaseActivity.ExceptionHandleType.LogOnlyError(getString(R.string.app_name), exception)
    }

    fun autoHandleError(autoHandle: Boolean) {
        autoErrorHandle = autoHandle
    }

    fun addFragment(@IdRes containerId: Int, fragment: Fragment, addToBackStack: Boolean = true) {
        with(supportFragmentManager.beginTransaction()) {
            if (addToBackStack) {
                addToBackStack(fragment.javaClass.simpleName)
            }
            setCustomAnimations(R.anim.slide_in, R.anim.slide_out, R.anim.slide_in, R.anim.slide_out)
            add(containerId, fragment)
            commit()
        }
    }

    fun replaceFragment(@IdRes containerId: Int, fragment: Fragment) {
        with(supportFragmentManager.beginTransaction()) {
            setCustomAnimations(R.anim.slide_in, R.anim.slide_out, R.anim.slide_in, R.anim.slide_out)
            replace(containerId, fragment)
            addToBackStack(null)
            commit()
        }
    }

    fun showProgress() {
    }

    fun hideProgress() {
    }

    sealed class ExceptionHandleType(val message: String, val throwable: Throwable) {
        class SnackBarError(message: String, throwable: Throwable) : ExceptionHandleType(message, throwable)
        class LogOnlyError(message: String, throwable: Throwable) : ExceptionHandleType(message, throwable)
        class ToastError(message: String, throwable: Throwable) : ExceptionHandleType(message, throwable)
        class DialogError(message: String, throwable: Throwable) : ExceptionHandleType(message, throwable)
        class DialogWithConfirmError(message: String, throwable: Throwable) : ExceptionHandleType(message, throwable)
    }


}

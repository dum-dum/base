package com.bosleo.feedback.presentation.interview

import android.animation.Animator
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import com.bosleo.feedback.R
import com.bosleo.feedback.presentation.candidate.CandidateActivity
import com.bosleo.feedback.presentation.core.BaseActivity
import com.bosleo.feedback.presentation.interview.adapter.InterviewPagerAdapter
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_COLOR
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_X
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_Y
import com.bosleo.feedback.utility.hideActivity
import com.bosleo.feedback.utility.revealActivity
import com.bosleo.feedback.utility.setupSlideWindowAnimations
import com.bosleo.feedback.utility.waitForLayout
import kotlinx.android.synthetic.main.activity_interview.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class InterviewActivity : BaseActivity<InterviewViewModel>() {

    private val interviewViewModel by viewModel<InterviewViewModel>()
    private var revealX: Float = 0f
    private var revealY: Float = 0f
    private var revealColor: Int = Color.WHITE
    private var isEnding = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_interview)

        init()
    }

    override fun getViewModel() = interviewViewModel

    private fun init() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupSlideWindowAnimations()
        }
        fabAddInterView.setOnClickListener {

        }

        setupInterviewViewPager()
        if (intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X)
            && intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)
        ) {


            revealX = intent.getFloatExtra(EXTRA_CIRCULAR_REVEAL_X, 0f);
            revealY = intent.getFloatExtra(EXTRA_CIRCULAR_REVEAL_Y, 0f);
            revealColor = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_COLOR, revealColor)

            clRoot.waitForLayout {
                //                progress.gone()
                clRoot.revealActivity(revealX, revealY, revealColor)
            }
        }
        fetchInterviews()

    }


    private fun setupInterviewViewPager() {
        vpInterview.adapter = InterviewPagerAdapter(supportFragmentManager)
        tabInterviews.setupWithViewPager(vpInterview)
    }

    private fun fetchInterviews() {
        interviewViewModel.interviewLiveData.observe(this, Observer {
            println("$it")
        })
        interviewViewModel.fetchAllInterviews()
    }

    override fun onBackPressed() {
        if (!isEnding) {
            isEnding = true
            clRoot.hideActivity(revealX, revealY, revealColor, object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                    finish()
                    overridePendingTransition(0, 0)
                }

                override fun onAnimationCancel(animation: Animator?) {
                }

                override fun onAnimationStart(animation: Animator?) {
                }

            })
        }
    }
}

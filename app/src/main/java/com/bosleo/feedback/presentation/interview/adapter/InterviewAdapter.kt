package com.bosleo.feedback.presentation.interview.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bosleo.feedback.R
import com.bosleo.feedback.model.Interview
import com.bosleo.feedback.utility.Constants
import com.bosleo.feedback.utility.setRandomBackground
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.cell_interview.*

class InterviewAdapter : RecyclerView.Adapter<InterviewAdapter.InterviewViewHolder>() {

    private var interviews: List<Interview> = arrayListOf()

    private lateinit var onInterviewClicked: (Interview,InterviewViewHolder) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): InterviewViewHolder {
        return InterviewViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_interview, parent, false))
    }

    override fun getItemCount() = interviews.size

    override fun onBindViewHolder(holder: InterviewViewHolder, position: Int) {
        holder.bind(interviews[position])
    }

    fun setData(interviews: List<Interview>) {
        this.interviews = interviews
        notifyDataSetChanged()
    }

    fun setOnInterviewClickListener(function: (Interview,InterviewViewHolder) -> Unit) {
        this.onInterviewClicked = function
    }

    inner class InterviewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

        init {
            clInterviewCell.setOnClickListener {
                if (::onInterviewClicked.isInitialized) {
                    onInterviewClicked(interviews[adapterPosition],this)
                }
            }
        }

        fun bind(interview: Interview) {
            with(interview) {
                ivAvatar.setRandomBackground()
                ivAvatar.setText(interview.candidate?.firstName?.first().toString())
                tvDate.text = interviewDate.toString(Constants.FORMAT_DDMMYYYY)
                tvComment.text = comment
                candidate?.apply {
                    tvName.text = String.format("%s %s", firstName, lastName)

                    tvDepartment.text = department.name

                    tvExperience.text = experience

                }
            }
        }
    }

}
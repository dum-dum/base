package com.bosleo.feedback.presentation.home

import androidx.lifecycle.MediatorLiveData
import com.bosleo.feedback.data.department.contract.DepartmentRepo
import com.bosleo.feedback.data.interview.contract.InterviewRepo
import com.bosleo.feedback.model.Interview
import com.bosleo.feedback.presentation.core.BaseViewModel
import com.bosleo.feedback.utility.Constants
import com.bosleo.feedback.utility.toDateTime
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.koin.standalone.inject

class HomeViewModel : BaseViewModel() {

    private val interviewRepo by inject<InterviewRepo>()

    private val departmentRepo by inject<DepartmentRepo>()

    val interviewLiveData = MediatorLiveData<List<Interview>>()

    fun fetchAllInterviews() {
        postLiveValue(interviewLiveData) { interviewRepo.getAllInterviews() }
    }

    fun getTodayInterviews(): List<Interview> {
        return interviewLiveData.value?.filter {
            it.interviewDate.withTimeAtStartOfDay() == DateTime().withTimeAtStartOfDay()
        }.orEmpty()
    }

    fun getUpcomingInterviews(): List<Interview> {
        return interviewLiveData.value?.filter {
            it.interviewDate.withTimeAtStartOfDay() > DateTime().withTimeAtStartOfDay()
        }?.sortedBy { it.interviewDate.millis }.orEmpty()
    }

    fun getFormDetails(departmentId: String) {
        launch {
            departmentRepo.getDepartmentWithForm(departmentId).either({
                    it.printStackTrace()
                },{
                    println("$it")
                })
        }
    }

}
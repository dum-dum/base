package com.bosleo.feedback.presentation.candidate

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.bosleo.feedback.data.candidate.contract.CandidateRepo
import com.bosleo.feedback.model.Candidate
import com.bosleo.feedback.presentation.core.BaseViewModel
import com.bosleo.feedback.utility.asMediatorLiveData
import org.koin.standalone.inject

class CandidateViewModel() : BaseViewModel() {

    private val candidateRepo: CandidateRepo by inject()
    val candidatesLiveData:MutableLiveData<List<Candidate>> by lazy { MediatorLiveData<List<Candidate>>() }

    fun fetchCandidates() {
        postLiveValue(candidatesLiveData.asMediatorLiveData()){
            candidateRepo.getAllCandidate()
        }
    }
}
package com.bosleo.feedback.presentation.interview.fragment

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bosleo.feedback.R
import com.bosleo.feedback.model.Interview
import com.bosleo.feedback.presentation.core.BaseFragment
import com.bosleo.feedback.presentation.interview.FeedbackActivity
import com.bosleo.feedback.presentation.interview.InterviewViewModel
import com.bosleo.feedback.presentation.interview.adapter.InterviewAdapter
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_COLOR
import com.bosleo.feedback.utility.Constants.EXTRA_ID
import com.bosleo.feedback.utility.Constants.EXTRA_INTERVIEW_ID
import com.bosleo.feedback.utility.Constants.EXTRA_NAME
import com.bosleo.feedback.utility.setVisibility
import kotlinx.android.synthetic.main.cell_interview.view.*
import kotlinx.android.synthetic.main.fragment_upcoming_interview.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class UpcomingInterviewFragment : BaseFragment() {

    private val interviewViewModel by sharedViewModel<InterviewViewModel>()

    private val interviewAdapter by lazy { InterviewAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_upcoming_interview, container, false)
        }
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        init()
    }

    private fun init() {
        setupTodayAdapter()
        attachObserver()
    }

    private fun setupTodayAdapter() {
        interviewAdapter.setOnInterviewClickListener { interview, interviewViewHolder ->
            interviewViewModel.getFormDetails(interview.candidate?.department?.id.orEmpty())
            startFeedbackActivity(interviewViewHolder, interview)
        }

        rvUpcomingInterview.adapter = interviewAdapter
        rvUpcomingInterview.layoutManager = LinearLayoutManager(activity)
    }

    private fun startFeedbackActivity(holder: InterviewAdapter.InterviewViewHolder, interview: Interview) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            val pairs = androidx.core.util.Pair(
                (holder?.itemView?.ivAvatar as View),
                getString(com.bosleo.feedback.R.string.transition_reveal1)
            )

            val i = Intent(activity, FeedbackActivity::class.java)
                .also {
                    it.putExtra(EXTRA_ID, interview.formId)
                    it.putExtra(EXTRA_NAME, "${interview.candidate?.firstName} ${interview.candidate?.lastName}")
                    it.putExtra(EXTRA_INTERVIEW_ID, interview.id)
//                    it.putExtra("sample", formAdapterItem.form)
                    it.putExtra(EXTRA_CIRCULAR_REVEAL_COLOR, holder.itemView?.ivAvatar?.avatarBackgroundColor)
                }

            val transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, pairs)
            startActivity(i, transitionActivityOptions.toBundle())
        } else {
            startActivity(Intent(activity, FeedbackActivity::class.java).also {
                it.putExtra(EXTRA_ID, interview.formId)
                it.putExtra(EXTRA_INTERVIEW_ID, interview.id)
                it.putExtra(EXTRA_NAME, "${interview.candidate?.firstName} ${interview.candidate?.lastName}")
            })
        }

    }

    private fun attachObserver() {
        interviewViewModel.interviewLiveData.observe(this, Observer {
            updateUI()
        })
    }

    private fun updateUI() {
        with(interviewViewModel.getUpcomingInterviews()) {
            interviewAdapter.setData(this)
            tvNoInterview.setVisibility(this.isEmpty())
        }
    }

}
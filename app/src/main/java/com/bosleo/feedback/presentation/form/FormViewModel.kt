package com.bosleo.feedback.presentation.form

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.bosleo.feedback.data.department.contract.DepartmentRepo
import com.bosleo.feedback.data.form.contract.FormRepo
import com.bosleo.feedback.presentation.core.BaseViewModel
import com.bosleo.feedback.presentation.form.adapter.FormAdapterItem
import com.bosleo.feedback.utility.asMediatorLiveData
import com.bosleo.feedback.utility.map
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.koin.standalone.inject
import java.lang.RuntimeException

class FormViewModel : BaseViewModel() {

    val formRepo by inject<FormRepo>()
    val departmentRepo by inject<DepartmentRepo>()

    val formAdapterLiveData: MutableLiveData<List<FormAdapterItem>> by lazy { MediatorLiveData<List<FormAdapterItem>>() }

     fun fetchDepartment() {
        postLiveValue(formAdapterLiveData.asMediatorLiveData()) {
            departmentRepo.getAllDepartments().map {
                Transformations.map(it) { list ->
                    val forms = arrayListOf<FormAdapterItem>()
                    runBlocking(Dispatchers.IO) {
                        list.map { departmet ->
                            formRepo.getForm(departmet.id).either({
                                throw RuntimeException("ERROR")
                            }, { form ->
                                forms.add(FormAdapterItem(form,departmet.name))
                            })
                        }
                    }
                    return@map forms.toList()
                }
            }

        }
    }
}
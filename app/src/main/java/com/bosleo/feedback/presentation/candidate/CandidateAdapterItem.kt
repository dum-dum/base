package com.bosleo.feedback.presentation.candidate

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bosleo.feedback.R
import com.bosleo.feedback.model.Candidate
import com.bosleo.feedback.utility.setRandomBackground
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.cell_candidate.view.*

class CandidateAdapterItem(val candidate: Candidate) :
    AbstractFlexibleItem<CandidateAdapterItem.CandidateViewHolder>() {

    var candidateViewHolder: CandidateViewHolder? = null
    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: CandidateViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        holder?.itemView?.apply {
            tvName.text = "${candidate.firstName} ${candidate.lastName}"
            tvDepartment.text = candidate.department.name
            ivAvatar.setText("${candidate.firstName.first().toUpperCase()}")
            ivAvatar.setRandomBackground()

        }
    }

    override fun equals(other: Any?) = other is Candidate && other.id == candidate.id

    override fun createViewHolder(
        view: View,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?
    ) = CandidateViewHolder(adapter,view)
    override fun getLayoutRes() = R.layout.cell_candidate

    override fun hashCode(): Int {
        return candidate.hashCode()
    }

    override fun unbindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: CandidateViewHolder?,
        position: Int
    ) {
        candidateViewHolder = null
        super.unbindViewHolder(adapter, holder, position)
    }
    override fun onViewAttached(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: CandidateViewHolder?,
        position: Int
    ) {
        super.onViewAttached(adapter, holder, position)
        candidateViewHolder = holder
    }
    inner class CandidateViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?, view: View) : FlexibleViewHolder(view,adapter)
}
package com.bosleo.feedback.presentation.interview

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.palette.graphics.Palette
import com.bosleo.feedback.R
import com.bosleo.feedback.presentation.core.BaseActivity
import com.bosleo.feedback.presentation.interview.adapter.MarksPagerViewAdapter
import com.bosleo.feedback.utility.*
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_COLOR
import com.bosleo.feedback.utility.Constants.EXTRA_ID
import com.bosleo.feedback.utility.Constants.EXTRA_INTERVIEW_ID
import com.bosleo.feedback.utility.Constants.EXTRA_NAME
import kotlinx.android.synthetic.main.activity_feedback.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class FeedbackActivity : BaseActivity<FeedbackViewModel>() {
    private val feedbackViewModel: FeedbackViewModel by viewModel()

    var revealColor: Int = Color.WHITE

    override fun getViewModel(): FeedbackViewModel {
        return feedbackViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)

        init()
        attachObserver()
    }

    private fun init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupRevealWindowAnimations(appbar, vpSkills, shared_target)
        } else {
            appbar.visible()
            shared_target.gone()
            vpSkills.visible()
        }
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        feedbackViewModel.interviewId = intent?.getStringExtra(
            EXTRA_INTERVIEW_ID
        ).orEmpty()

        feedbackViewModel.fetchFormData(
            intent?.getStringExtra(EXTRA_ID).orEmpty()
        )

        supportActionBar?.title = intent?.getStringExtra(EXTRA_NAME).orEmpty()
        revealColor = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_COLOR, revealColor)

        toolbar.setBackgroundColor(revealColor)

        Palette.from(toolbar.layoutToBitmap()).generate().vibrantSwatch

        tabs.setBackgroundColor(revealColor)
        shared_target.setColorFilter(revealColor, android.graphics.PorterDuff.Mode.MULTIPLY);




        fab.setOnClickListener {
            feedbackViewModel.saveData()
            startActivity(Intent(this,RemarksActivity::class.java).also { intent ->
                intent.putExtra(EXTRA_INTERVIEW_ID,feedbackViewModel.interviewId)
                intent.putExtra(EXTRA_CIRCULAR_REVEAL_COLOR,revealColor)
            })
        }
    }

    private fun attachObserver() {
        feedbackViewModel.formLiveData.observe(this, Observer {
            it?.apply {
                vpSkills.adapter =
                        MarksPagerViewAdapter(this.skills, revealColor.manipulateColor(.6f), feedbackViewModel)
                tabs.setupWithViewPager(vpSkills)
            }
        })
    }

    override fun onBackPressed() {
        feedbackViewModel.saveData()
        super.onBackPressed()
    }
}

package com.bosleo.feedback.presentation.sample

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bosleo.feedback.R
import com.bosleo.feedback.api.Post
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.cell_post.*

class PostAdapter(private var posts: List<Post>) : androidx.recyclerview.widget.RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): PostViewHolder {
        return PostViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_post, parent, false))
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(posts[position])
    }

    fun setData(posts: List<Post>) {
        this.posts = posts
        notifyDataSetChanged()
    }


    class PostViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView: View?
            get() = itemView

        fun bind(post: Post) {
            tvPostId.text = post.id.toString()
            tvUserId.text = post.userId.toString()
            tvTitle.text = post.title
            tvBody.text = post.body
        }

    }

}
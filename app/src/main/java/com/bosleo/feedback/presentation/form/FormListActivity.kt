package com.bosleo.feedback.presentation.form

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bosleo.feedback.presentation.core.BaseActivity
import com.bosleo.feedback.presentation.form.adapter.FormAdapterItem
import com.bosleo.feedback.utility.*
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_COLOR
import com.bosleo.feedback.utility.Constants.EXTRA_ID
import com.bosleo.feedback.utility.Constants.EXTRA_NAME
import com.github.abdularis.civ.AvatarImageView
import eu.davidea.flexibleadapter.FlexibleAdapter
import kotlinx.android.synthetic.main.activity_form_list.*
import kotlinx.android.synthetic.main.cell_department.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class FormListActivity : BaseActivity<FormViewModel>() {

    var isEnding = false

    val formViewModel by viewModel<FormViewModel>()

    private var revealX: Float = 0f
    private var revealY: Float = 0f
    private var revealColor: Int = Color.WHITE


    override fun getViewModel(): FormViewModel = formViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.bosleo.feedback.R.layout.activity_form_list)

        init()
        attachObserver()

        if (savedInstanceState == null
            && intent.hasExtra(Constants.EXTRA_CIRCULAR_REVEAL_X)
            && intent.hasExtra(Constants.EXTRA_CIRCULAR_REVEAL_Y)
        ) {


            revealX = intent.getFloatExtra(Constants.EXTRA_CIRCULAR_REVEAL_X, 0f);
            revealY = intent.getFloatExtra(Constants.EXTRA_CIRCULAR_REVEAL_Y, 0f);
            revealColor = intent.getIntExtra(Constants.EXTRA_CIRCULAR_REVEAL_COLOR, revealColor)

            clRoot.waitForLayout {
                progress.gone()
                clRoot.revealActivity(revealX, revealY, revealColor)
            }
        }
    }

    private fun attachObserver() {

        formViewModel.fetchDepartment()
        formViewModel.formAdapterLiveData.observe(this, Observer {

            it?.apply {
                @Suppress("UNCHECKED_CAST")
                (rvForm.adapter as FlexibleAdapter<FormAdapterItem>).updateDataSet(this.toMutableList(), true)
            }
        })
    }

    private fun init() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupSlideWindowAnimations()
        }
        rvForm.layoutManager = LinearLayoutManager(this@FormListActivity)
        rvForm.adapter = FlexibleAdapter<FormAdapterItem>(arrayListOf()).also {
            it.isAnimateChangesWithDiffUtil = true
            it.setAnimationEntryStep(true)
            it.mItemClickListener = FlexibleAdapter.OnItemClickListener { _, position ->
                it.getItem(position)?.apply {
                    startFormDetailActivity(this)
                }
                return@OnItemClickListener true
            }
        }

        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(com.bosleo.feedback.R.string.form)
        fabSave.setOnClickListener { _ ->

            //            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, fabSave, "transitionCandidate")
//            val rect = Rect()
//            fabSave.getGlobalVisibleRect(rect)
//
//            val revealX: Float = (rect.left + fabSave.width / 2).toFloat()
//            val revealY: Float = (rect.top + fabSave.height / 2).toFloat()
//
//            val intent = Intent(this, AddCandidateActivity::class.java)
//            intent.putExtra(EXTRA_CIRCULAR_REVEAL_X, revealX)
//            intent.putExtra(EXTRA_CIRCULAR_REVEAL_Y, revealY)
//            intent.putExtra(EXTRA_CIRCULAR_REVEAL_COLOR, ContextCompat.getColor(this, R.color.colorAccent))
//
//            ActivityCompat.startActivity(this, intent, options.toBundle())

        }


    }

    private fun startFormDetailActivity(formAdapterItem: FormAdapterItem) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            val pairs = androidx.core.util.Pair(
                ((formAdapterItem.viewHolder?.itemView?.flipView?.frontLayout as AvatarImageView?) as View),
                getString(com.bosleo.feedback.R.string.transition_reveal1)
            )

            val i = Intent(this, FormDetailActivity::class.java)
                .also {
                    it.putExtra(EXTRA_ID, formAdapterItem.form.id)
                    it.putExtra(EXTRA_NAME, formAdapterItem.departmentName)
                    it.putExtra("sample", formAdapterItem.form)
                    it.putExtra(
                        EXTRA_CIRCULAR_REVEAL_COLOR,
                        (formAdapterItem.viewHolder?.itemView?.flipView?.frontLayout as AvatarImageView?)?.avatarBackgroundColor
                    )
                }

            val transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs)
            startActivity(i, transitionActivityOptions.toBundle())
        } else {
            startActivity(Intent(this, FormDetailActivity::class.java).also {
                it.putExtra(EXTRA_ID, formAdapterItem.form.id)
                it.putExtra(EXTRA_NAME, formAdapterItem.departmentName)
            })
        }

    }


}

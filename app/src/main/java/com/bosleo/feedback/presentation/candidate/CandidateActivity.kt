package com.bosleo.feedback.presentation.candidate

import android.animation.Animator
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bosleo.feedback.R
import com.bosleo.feedback.presentation.core.BaseActivity
import com.bosleo.feedback.presentation.form.FormDetailActivity
import com.bosleo.feedback.utility.*
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_COLOR
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_X
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_Y
import com.bosleo.feedback.utility.Constants.EXTRA_ID
import eu.davidea.flexibleadapter.FlexibleAdapter
import kotlinx.android.synthetic.main.activity_candidate_list.*
import kotlinx.android.synthetic.main.cell_candidate.view.*

import org.koin.android.ext.android.inject

class CandidateActivity : BaseActivity<CandidateViewModel>() {

    var isEnding = false

    private val candidateViewModel by inject<CandidateViewModel>()

    override fun getViewModel() = candidateViewModel


    private var revealX: Float = 0f
    private var revealY: Float = 0f
    private var revealColor: Int = Color.WHITE


    override fun getLoadingView(): View? {
        return progress
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_candidate_list)

        init()
        attachObserver()



        if (savedInstanceState == null
            && intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X)
            && intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)
        ) {


            revealX = intent.getFloatExtra(EXTRA_CIRCULAR_REVEAL_X, 0f);
            revealY = intent.getFloatExtra(EXTRA_CIRCULAR_REVEAL_Y, 0f);
            revealColor = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_COLOR, revealColor)

            clRoot.waitForLayout {
                progress.gone()
                clRoot.revealActivity(revealX, revealY, revealColor)
            }
        }
    }


    private fun attachObserver() {
        candidateViewModel.candidatesLiveData.observe(this, Observer {

            it?.apply {
                @Suppress("UNCHECKED_CAST")
                (rvCandidate.adapter as FlexibleAdapter<CandidateAdapterItem>).updateDataSet(this.toMutableList().map {
                    CandidateAdapterItem(it)
                }, true)
            }
        })
    }


    private fun init() {
        rvCandidate.layoutManager = LinearLayoutManager(this@CandidateActivity)
        rvCandidate.adapter = FlexibleAdapter<CandidateAdapterItem>(arrayListOf()).also {
            it.isAnimateChangesWithDiffUtil = true
            it.setAnimationEntryStep(true)
            it.mItemClickListener = FlexibleAdapter.OnItemClickListener { _, position ->
                startEditDetailActivity(it.getItem(position) as CandidateAdapterItem)
                return@OnItemClickListener true
            }
        }

        setSupportActionBar(toolbar)

        fabSave.setOnClickListener { _ ->


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                val pairs = androidx.core.util.Pair(
                    fabSave as View,
                    getString(com.bosleo.feedback.R.string.transition_reveal1)
                )

                val i = Intent(this, AddCandidateActivity::class.java)
                    .also {
                        it.putExtra(
                            EXTRA_CIRCULAR_REVEAL_COLOR,
                            ContextCompat.getColor(this, R.color.orange)
                        )
                    }

                val transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs)
                startActivity(i, transitionActivityOptions.toBundle())
            } else {
                startActivity(Intent(this, AddCandidateActivity::class.java).also {
                })
            }

        }







        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        candidateViewModel.fetchCandidates()
    }


    override fun onBackPressed() {
        if (!isEnding) {
            isEnding = true
            clRoot.hideActivity(revealX, revealY, revealColor, object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                    finish()
                    overridePendingTransition(0, 0)

                }

                override fun onAnimationCancel(animation: Animator?) {
                }

                override fun onAnimationStart(animation: Animator?) {
                }

            })
        }
    }

    override fun onProgressComplete() {
        hideProgress()
//        finish()
    }


    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupSlideWindowAnimations()
        }

    }


    private fun startEditDetailActivity(candidateAdapterItem: CandidateAdapterItem) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            val pairs = androidx.core.util.Pair(
                (candidateAdapterItem.candidateViewHolder?.itemView?.ivAvatar as View),
                getString(com.bosleo.feedback.R.string.transition_reveal1)
            )

            val i = Intent(this, AddCandidateActivity::class.java)
                .also {
                    it.putExtra(EXTRA_ID, candidateAdapterItem.candidate.id)
                    it.putExtra(
                        EXTRA_CIRCULAR_REVEAL_COLOR,
                        candidateAdapterItem.candidateViewHolder?.itemView?.ivAvatar?.avatarBackgroundColor
                    )
                }

            val transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs)
            startActivity(i, transitionActivityOptions.toBundle())
        } else {
            startActivity(Intent(this, AddCandidateActivity::class.java).also {
                it.putExtra(EXTRA_ID, candidateAdapterItem.candidate.id)
            })
        }

    }

}

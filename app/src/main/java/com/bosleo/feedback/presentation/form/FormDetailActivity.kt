package com.bosleo.feedback.presentation.form

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.view.ActionMode
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.bosleo.feedback.R
import com.bosleo.feedback.presentation.core.BaseActivity
import com.bosleo.feedback.presentation.form.adapter.SkillAdapterItem
import com.bosleo.feedback.presentation.form.adapter.SkillPagerViewAdapter
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_COLOR
import com.bosleo.feedback.utility.Constants.EXTRA_ID
import com.bosleo.feedback.utility.Constants.EXTRA_NAME
import com.bosleo.feedback.utility.gone
import com.bosleo.feedback.utility.setupRevealWindowAnimations
import com.bosleo.feedback.utility.showSnackBar
import com.bosleo.feedback.utility.visible
import com.leinardi.android.speeddial.SpeedDialActionItem
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.SelectableAdapter
import eu.davidea.flexibleadapter.helpers.ActionModeHelper
import kotlinx.android.synthetic.main.activity_form_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class FormDetailActivity : BaseActivity<FormDetailViewModel>(), ActionMode.Callback,
    FlexibleAdapter.OnItemClickListener,
    FlexibleAdapter.OnItemLongClickListener {

    val formDetailViewModel by viewModel<FormDetailViewModel>()
    private var skillPagerAdapter: SkillPagerViewAdapter? = null
    private var actionModeHelper: ActionModeHelper? = null
    private var adapter: FlexibleAdapter<SkillAdapterItem>? = null
    private var revealColor: Int = Color.BLACK

    override fun getViewModel() = formDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        attachObserver()
        init()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupRevealWindowAnimations(appbar, vpSkills, shared_target)
        }
//        fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
//        }

    }


    private fun init() {
        formDetailViewModel.fetchFormData(intent?.getStringExtra(EXTRA_ID).orEmpty())
        supportActionBar?.title = intent?.getStringExtra(EXTRA_NAME).orEmpty()
        revealColor = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_COLOR, revealColor)
        toolbar.setBackgroundColor(revealColor)
        appbar.setBackgroundColor(revealColor)

        tabs.setBackgroundColor(revealColor)
        shared_target.setColorFilter(revealColor, android.graphics.PorterDuff.Mode.MULTIPLY)


        fab.addActionItem(
            SpeedDialActionItem.Builder(com.bosleo.feedback.R.id.fabAddSkill, com.bosleo.feedback.R.drawable.ic_add)
                .setLabel(com.bosleo.feedback.R.string.skill)
                .setFabBackgroundColor(
                    ResourcesCompat.getColor(
                        resources,
                        com.bosleo.feedback.R.color.colorAccent,
                        theme
                    )
                )
                .setFabImageTintColor(
                    ResourcesCompat.getColor(
                        resources,
                        com.bosleo.feedback.R.color.white,
                        theme
                    )
                )
                .setLabelClickable(true)
                .create()
        )

        fab.addActionItem(
            SpeedDialActionItem.Builder(com.bosleo.feedback.R.id.fabAddSubSkill, com.bosleo.feedback.R.drawable.ic_add)
                .setLabel(com.bosleo.feedback.R.string.sub_skill)
                .setFabBackgroundColor(
                    ResourcesCompat.getColor(
                        resources,
                        com.bosleo.feedback.R.color.colorAccent,
                        theme
                    )
                )
                .setFabImageTintColor(
                    ResourcesCompat.getColor(
                        resources,
                        com.bosleo.feedback.R.color.white,
                        theme
                    )
                )
                .setLabelClickable(true)
                .create()
        )
        fab.addActionItem(
            SpeedDialActionItem.Builder(R.id.fabDeleteSkill, R.drawable.ic_delete)
                .setLabel(R.string.delete_skill)
                .setFabBackgroundColor(ResourcesCompat.getColor(resources, R.color.colorAccent, theme))
                .setFabImageTintColor(ResourcesCompat.getColor(resources, R.color.white, theme))
                .create()
        )

        fab.setOnActionSelectedListener {

            when (it.id) {
                com.bosleo.feedback.R.id.fabAddSkill -> {
                    val type = InputType.TYPE_CLASS_TEXT
                    MaterialDialog(this)
                        .show {
                            title(com.bosleo.feedback.R.string.enter_skill_header)
                            input(inputType = type, waitForPositiveButton = true)
                            positiveButton(com.bosleo.feedback.R.string.add) {
                                formDetailViewModel.addSkill(getInputField().text.toString())
                            }
                        }
                }
                com.bosleo.feedback.R.id.fabAddSubSkill -> {
                    if (formDetailViewModel.formLiveData.value?.skills?.isEmpty() == true) {
                        showSnackBar(getString(R.string.add_skill_first))
                        return@setOnActionSelectedListener false
                    } else {
                        val type = InputType.TYPE_CLASS_TEXT
                        MaterialDialog(this)
                            .show {
                                title(com.bosleo.feedback.R.string.enter_sub_skill_header)
                                input(inputType = type, waitForPositiveButton = true)
                                positiveButton(com.bosleo.feedback.R.string.add) {
                                    (this@FormDetailActivity.vpSkills.adapter as SkillPagerViewAdapter).getItemAtPosition(
                                        this@FormDetailActivity.vpSkills.currentItem
                                    )
                                        ?.apply {
                                            formDetailViewModel.addSubSkill(
                                                getInputField().text.toString(),
                                                this.skill.id
                                            )
                                        }
                                }
                            }
                    }
                }
                R.id.fabDeleteSkill -> {
                    if (formDetailViewModel.formLiveData.value?.skills?.isEmpty() == true) {
                        showSnackBar(getString(R.string.add_skill_first))
                        return@setOnActionSelectedListener false
                    } else {
                        MaterialDialog(this)
                            .show {
                                title(R.string.delete_sub_skill)
                                positiveButton(R.string.yes) {
                                    val skillId =
                                        (this@FormDetailActivity.vpSkills.adapter as SkillPagerViewAdapter).getItemAtPosition(
                                            this@FormDetailActivity.vpSkills.currentItem
                                        )
                                            ?.skill?.id
                                    formDetailViewModel.deleteSkill(skillId.orEmpty())
                                }
                                negativeButton(R.string.no) { dismiss() }
                            }
                    }
                }
            }

            return@setOnActionSelectedListener false
        }

        vpSkills.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                actionModeHelper?.destroyActionModeIfCan()
                initializeActionModeHelper(SelectableAdapter.Mode.MULTI)
            }
        })
    }

    private fun attachObserver() {
        observeFormLiveData()
        observeDeleteSubSkillLiveData()
        observeDeleteSkillLiveData()
        observeAddSubSkillLiveData()
    }

    private fun observeFormLiveData() {
        formDetailViewModel.formLiveData.observe(this, Observer {
            it?.apply {
                if (skillPagerAdapter == null) {
                    skillPagerAdapter = SkillPagerViewAdapter(it.skills)
                    skillPagerAdapter?.onSubSkillImageClick { pos, isFlipped ->

                        actionModeHelper?.onLongClick(this@FormDetailActivity, pos)

                        if (adapter?.selectedItemCount != 0) {
                            //remove below comment if,wants to disable swipe in viewpager
                            //vpSkills.setPagingEnabled(false)
                            tabs.pivotY = 0f
                            //tabs.animate().scaleY(0f).setDuration(300).start()
                            tabs.animate().translationY(-56f).setDuration(200).start()
                            Handler().postDelayed({ tabs.gone() }, 200)
                        } else {
                            vpSkills.setPagingEnabled(true)
                        }
                    }
                    vpSkills.adapter = skillPagerAdapter
                    tabs.setupWithViewPager(vpSkills)
                    if (vpSkills.currentItem == 0) {
                        initializeActionModeHelper(SelectableAdapter.Mode.MULTI)
                    }
                } else {
                    skillPagerAdapter?.setSkills(this.skills)
                }
            }
        })
    }

    private fun observeDeleteSubSkillLiveData() {
        formDetailViewModel.deleteSubSkillLiveData.observe(this, Observer {
            it?.let { deletedSubSkillId ->
                actionModeHelper?.destroyActionModeIfCan()
                if (deletedSubSkillId.isNotEmpty()) {
                    adapter?.currentItems?.filter {
                        it?.subSkill?.id !in deletedSubSkillId
                    }?.apply {
                        adapter?.updateDataSet(this)
                    }
                }
            }
        })
    }

    private fun observeAddSubSkillLiveData() {
        formDetailViewModel.addSubSkillLiveData.observe(this, Observer {
            it?.let {
                actionModeHelper?.destroyActionModeIfCan()
                if (vpSkills.currentItem == 0) {
                    initializeActionModeHelper(SelectableAdapter.Mode.MULTI)
                }
                adapter?.addItem(SkillAdapterItem(it))
            }
        })
    }

    private fun observeDeleteSkillLiveData() {
        formDetailViewModel.deleteSkillLiveData.observe(this, Observer {
            it?.let { skillId ->
                actionModeHelper?.destroyActionModeIfCan()
            }
        })
    }

    private fun initializeActionModeHelper(@SelectableAdapter.Mode mode: Int) {
        vpSkills?.currentItem?.let {
            adapter = (vpSkills?.adapter as? SkillPagerViewAdapter)?.getRecyclerAdapter(it)
            adapter?.let {
                it.addListener(this)
                actionModeHelper = object : ActionModeHelper(it, R.menu.menu_form_detail, this) {
                    override fun updateContextTitle(count: Int) {
                        actionMode?.title = "selected item $count"
                    }
                }.withDefaultMode(mode)
            }
        }
    }


    override fun onItemClick(view: View?, position: Int): Boolean {
        return if (actionModeHelper != null) {
            if(adapter?.mode!=SelectableAdapter.Mode.IDLE){
                adapter?.notifyItemChanged(position)
            }
            val activate = actionModeHelper?.onClick(position)
            actionModeHelper?.updateContextTitle(adapter?.selectedItemCount ?: 0)
            activate ?: false
        } else {
            false
        }
    }

    override fun onItemLongClick(position: Int) {
        actionModeHelper?.onLongClick(this@FormDetailActivity, position)
        if (adapter?.selectedItemCount != 0) {
            //remove below comment if,wants to disable swipe in viewpager
            //vpSkills.setPagingEnabled(false)
            tabs.pivotY = 0f
            //tabs.animate().scaleY(0f).setDuration(300).start()
            adapter?.notifyItemChanged(position)
            tabs.animate().translationY(-56f).setDuration(200).start()
            Handler().postDelayed({ tabs.gone() }, 200)
        } else {
            vpSkills.setPagingEnabled(true)
        }
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menuItemDelete -> {
                val subSkills = getSelectedItemsAndDestroyActionMode()
                formDetailViewModel.deleteSubSkills(subSkills.first().subSkill.skillId, subSkills.map {
                    it.subSkill.id
                })
            }
        }
        return true
    }

    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        return true
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        tabs.pivotY = 0f
        tabs.visible()
        //tabs.animate().scaleY(1f).setDuration(300).start()
        vpSkills.setPagingEnabled(true)
        adapter?.notifyDataSetChanged()
        tabs.animate().translationY(0f).setDuration(300).start()
    }

    private fun getSelectedItemsAndDestroyActionMode(): MutableList<SkillAdapterItem> {
        val selectedItemPositions = adapter?.selectedPositions.orEmpty()
        val selectedItems: MutableList<SkillAdapterItem> = arrayListOf()
        adapter?.currentItems?.forEachIndexed { index, skillAdapterItem ->
            if (index in selectedItemPositions) {
                selectedItems.add(skillAdapterItem)
            }
        }
        adapter?.clearSelection()
        actionModeHelper?.destroyActionModeIfCan()
        return selectedItems
    }

}

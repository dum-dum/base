package com.bosleo.feedback.presentation.interview

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.bosleo.feedback.data.form.contract.FormRepo
import com.bosleo.feedback.data.marks.contract.InterviewMarksRepo
import com.bosleo.feedback.model.Form
import com.bosleo.feedback.model.InterviewMark
import com.bosleo.feedback.model.SubSkill
import com.bosleo.feedback.presentation.core.BaseViewModel
import com.bosleo.feedback.utility.asMediatorLiveData
import com.bosleo.feedback.utility.flatMap
import com.bosleo.feedback.utility.map
import org.koin.standalone.inject

class FeedbackViewModel : BaseViewModel() {

    val formRepo by inject<FormRepo>()

    lateinit var interviewId: String

    val formLiveData: MutableLiveData<Form> by lazy { MediatorLiveData<Form>() }

    val interviewMarksRepo: InterviewMarksRepo by inject()

    var marks: MutableList<InterviewMark> = mutableListOf()

    fun fetchFormData(formId: String) {


        postLiveValue(formLiveData.asMediatorLiveData()) {

            interviewMarksRepo.getMarksForInterView(interviewId)
                .flatMap {
                    marks = it.toMutableList()
                    formRepo.getFormWithSkills(formId)
                }
        }

    }

    fun saveData() {
        postValue(loadingLiveData) {
            interviewMarksRepo.addMark(marks)
                .map {
                    marks = it.toMutableList()
                    true
                }
        }
    }

    fun updateMarkToViewModel(subSkill: SubSkill, mark: Int) {
        marks.find {
            it.subSkillId == subSkill.id
        }?.apply {
            this.mark = mark
        } ?: kotlin.run {
            marks.add(InterviewMark("", subSkill.skillId, interviewId, subSkill.id, mark))
        }
    }

}
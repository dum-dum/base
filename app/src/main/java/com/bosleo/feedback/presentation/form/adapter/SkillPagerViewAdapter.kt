package com.bosleo.feedback.presentation.form.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.bosleo.feedback.model.SkillWithSubSkill
import com.bosleo.feedback.utility.replaceAll
import eu.davidea.flexibleadapter.FlexibleAdapter

class SkillPagerViewAdapter(private var skills: List<SkillWithSubSkill>) : PagerAdapter() {

    private var recyclerList = arrayOfNulls<RecyclerView>(skills.size).toMutableList()

    private lateinit var onSubSkillImgClick: (Int, Boolean) -> Unit

    private var container: ViewGroup? = null

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        this.container = container
        val recyclerView = RecyclerView(container.context)
        recyclerView.layoutManager = LinearLayoutManager(container.context)
        recyclerView.adapter = FlexibleAdapter<SkillAdapterItem>(skills[position].subSkill.map {
            SkillAdapterItem(it).also {
                it.onImageClick { pos, isFlipped ->
                    if (::onSubSkillImgClick.isInitialized) {
                        onSubSkillImgClick(skills[position].subSkill.indexOf(pos), isFlipped)
                    }
                }
            }
        })
        recyclerList[position] = recyclerView
        container.addView(recyclerView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        return recyclerView
    }

    fun getRecyclerAdapter(position: Int): FlexibleAdapter<SkillAdapterItem>? {
        return if (recyclerList.size <= position) {
            null
        } else {
            val adapter = recyclerList[position]?.adapter
            (adapter as? FlexibleAdapter<SkillAdapterItem>).also {
                it?.currentItems?.map {
                    it.onImageClick { pos, isFlipped ->
                        if (::onSubSkillImgClick.isInitialized) {
                            onSubSkillImgClick(skills[position].subSkill.indexOf(pos), isFlipped)
                        }
                    }
                }
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return skills[position].skill.name
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        if (recyclerList.size > position)
            recyclerList[position] = null
        container.removeView(`object` as View)
    }

    override fun getItemPosition(`object`: Any) = POSITION_NONE

    fun getItemAtPosition(position: Int): SkillWithSubSkill? {
        return if (position < skills.size) {
            skills[position]
        } else {
            null
        }
    }

    override fun getCount() = skills.size

    fun onSubSkillImageClick(onSubSkillImgClick: (Int, Boolean) -> Unit) {
        this.onSubSkillImgClick = onSubSkillImgClick
    }

    fun setSkills(skills: MutableList<SkillWithSubSkill>) {
        if (skills.isEmpty()) {
            container?.removeAllViews()
            recyclerList = arrayListOf()
        }
        val tempList = recyclerList
        recyclerList = arrayOfNulls<RecyclerView>(skills.size).toMutableList()

        if (tempList.size > skills.size) {
            recyclerList.replaceAll(tempList)
        } else {
            tempList.forEachIndexed { index, recyclerView ->
                recyclerList[index] = recyclerView
            }
        }
        this.skills = skills
        notifyDataSetChanged()
    }
}
package com.bosleo.feedback.presentation.candidate

import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bosleo.feedback.R
import com.bosleo.feedback.databinding.ActivityAddCandidateBinding
import com.bosleo.feedback.model.Department
import com.bosleo.feedback.presentation.core.BaseActivity
import com.bosleo.feedback.utility.*
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_COLOR
import com.bosleo.feedback.utility.Constants.EXTRA_ID
import kotlinx.android.synthetic.main.activity_add_candidate.*
import org.joda.time.DateTime
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddCandidateActivity : BaseActivity<AddCandidateViewModel>() {

    var isEnding = false

    private val candidateViewModel by viewModel<AddCandidateViewModel>()

    override fun getViewModel() = candidateViewModel

    lateinit var candidateBinding: ActivityAddCandidateBinding

    private lateinit var departmentAdapter: ArrayAdapter<Department>

    private var revealX: Float = 0f
    private var revealY: Float = 0f
    private var revealColor: Int = Color.WHITE

    override fun getLoadingView(): View? {
        return progress
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        candidateBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_candidate)

        init()
        attachObserver()


    }


    private fun attachObserver() {
        candidateViewModel.departmentLiveData.observe(this, Observer {
            it?.apply {
                departmentAdapter.clear()
                departmentAdapter.addAll(this.toMutableList().also {
                    it.add(Department("", "Other"))
                })
            }
        })

        candidateViewModel.candidateLiveData.observe(this, Observer {

            it?.apply {
                candidateBinding.candidate = it.candidate
                this@AddCandidateActivity.etInterviewDate.setText( interview.interviewDate.toString(Constants.FORMAT_DDMMYYYY))
            }
        })

    }


    private fun init() {
        candidateBinding.candidate = candidateViewModel.candidate

        setSupportActionBar(toolbar)

        candidateViewModel.fetchDepartments()


        candidateViewModel.candidateId = intent.getStringExtra(EXTRA_ID).orEmpty()
        candidateViewModel.fetchCandidate()
        revealColor = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_COLOR, revealColor)
        clRoot.setBackgroundColor(Color.WHITE)
        clRoot.waitForLayout {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                scrollView.gone()
                shared_target.setColorFilter(revealColor, android.graphics.PorterDuff.Mode.MULTIPLY);

                toolbar.setBackgroundColor(revealColor)
                setupRevealWindowAnimations(appbar, scrollView, shared_target)
            }
        }






        fabSave.setOnClickListener {
            var isError = false
            if (etFirstName.text.toString().isBlank()) {
                etFirstName.error = "First name can not be blank"
                isError = true
            }
            if (etLastName.text.toString().isBlank()) {
                etLastName.error = "First name can not be blank"
                isError = true
            }
            if (etInterviewDate.text.toString().isBlank()) {
                etInterviewDate.error = "Please select interview date first"
                isError = true
            }

            if (spDepartment.selectedItem == null) {
                spDepartment.error = "Please select department first"
                isError = true
            } else if ((spDepartment.selectedItem as Department).id.isBlank() && etOtherDepartment.text.toString().isBlank()) {
                etOtherDepartment.error = "Please enter other department name"
                isError = true
            }
            if (isError) {
                return@setOnClickListener
            }
            candidateViewModel.insertCandidate()
        }

        val today = DateTime.now()
        etInterviewDate.setOnClickListener {
            DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    candidateViewModel.selectedDate = DateTime()
                        .withYear(year)
                        .withMonthOfYear(month + 1)
                        .withDayOfMonth(dayOfMonth)

                    candidateViewModel.selectedDate
                        .toString(Constants.FORMAT_DDMMYYYY)
                        .apply {
                            etInterviewDate.setText(this)
                        }
                },
                today.year,
                today.monthOfYear - 1,
                today.dayOfMonth
            ).show()
        }
        departmentAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayListOf())
        spDepartment.adapter = departmentAdapter
        spDepartment.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position > -1) {
                    departmentAdapter.getItem(position)?.let {
                        candidateViewModel.selectedDepartment = it
                        if (it.id.isBlank()) {
                            etOtherDepartment.visible()
                        } else {
                            etOtherDepartment.gone()
                            candidateViewModel.candidate.department = it
                        }
                    }
                }


            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }



        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }


    override fun onProgressComplete() {
        hideProgress()
//        finish()
    }


}

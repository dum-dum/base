package com.bosleo.feedback.presentation.login

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.bosleo.feedback.R
import com.bosleo.feedback.presentation.core.BaseActivity
import com.bosleo.feedback.presentation.splash.SplashActivity
import com.bosleo.feedback.utility.Constants
import com.bosleo.feedback.utility.isValidEmail
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : BaseActivity<LoginViewModel>() {

    private val loginViewModel by viewModel<LoginViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
        attachObserver()
    }

    override fun onProgressStart() {
        btnLogin.startAnimation()
    }

    override fun onProgressComplete() {
        btnLogin.revertAnimation {
            btnLogin.isEnabled = true
        }
    }

    private fun attachObserver() {
        loginViewModel.userLiveData.observe(this, Observer {
            it?.apply {
                if (Prefs.getString(Constants.PrefKey.TOKEN, "").isNotBlank()) {
                    startActivity(Intent(this@LoginActivity, SplashActivity::class.java))
                    finish()
                }else{
                    onProgressComplete()
                }
            }?:onProgressComplete()
        })
    }

    override fun getViewModel() = loginViewModel


    private fun init() {
        loginViewModel.getUser()

        btnLogin.setOnClickListener {
            if (isValidDetails()) {
                loginViewModel.doLogin(etEmail.text.toString(), etPassword.text.toString())
//                loginViewModel.doLogin("admin@bosleo.com", "bosleo")
            }
        }
    }

    private fun isValidDetails(): Boolean {
        if (etEmail.text.isNullOrBlank() || !etEmail.text.toString().isValidEmail()) {
            etEmail.error = "Please enter valid email"
            return false
        }

        if (etPassword.text.isNullOrBlank()) {
            etEmail.error = "Please enter password"
            return false
        }
        return true
    }

}

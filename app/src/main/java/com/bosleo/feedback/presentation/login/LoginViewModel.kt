package com.bosleo.feedback.presentation.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.bosleo.feedback.data.login.contract.LoginRepo
import com.bosleo.feedback.model.User
import com.bosleo.feedback.presentation.core.BaseViewModel
import com.bosleo.feedback.utility.asMediatorLiveData
import com.bosleo.feedback.utility.map
import org.koin.standalone.inject

class LoginViewModel : BaseViewModel() {

    private val loginRepo by inject<LoginRepo>()

    val userLiveData: LiveData<User?> by lazy { MediatorLiveData<User?>() }

    fun doLogin(email: String, password: String) {
        postValue(loadingLiveData) {
            loginRepo.doLogin(email, password).map {
                false
            }
        }
    }

    fun getUser() {
        postLiveValue(userLiveData.asMediatorLiveData()) {
            loginRepo.getUser()
        }
    }
}
package com.bosleo.feedback.presentation.splash

import com.bosleo.feedback.data.syncmaster.MasterRepo
import com.bosleo.feedback.presentation.core.BaseViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.rx2.await

class SplashViewModel(private val masterRepo: MasterRepo) : BaseViewModel() {

    fun startSync() {
        loadingLiveData.postValue(true)
        launch {
            try {
                masterRepo.fetchMasters().await()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            loadingLiveData.postValue(false)
        }
    }

}
package com.bosleo.feedback.presentation.candidate

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.bosleo.feedback.data.candidate.contract.CandidateRepo
import com.bosleo.feedback.data.department.contract.DepartmentRepo
import com.bosleo.feedback.data.form.contract.FormRepo
import com.bosleo.feedback.data.interview.contract.InterviewRepo
import com.bosleo.feedback.model.Candidate
import com.bosleo.feedback.model.CandidateWithInterview
import com.bosleo.feedback.model.Department
import com.bosleo.feedback.model.Interview
import com.bosleo.feedback.presentation.core.BaseViewModel
import com.bosleo.feedback.utility.Either
import com.bosleo.feedback.utility.flatMap
import com.bosleo.feedback.utility.map
import org.joda.time.DateTime
import org.koin.standalone.inject

class AddCandidateViewModel() : BaseViewModel() {

    private val candidateRepo: CandidateRepo by inject()
    private val departmentRepo: DepartmentRepo by inject()
    private val interviewDataRepo: InterviewRepo by inject()
    private val formDataRepo: FormRepo by inject()
   var candidateId: String =""

    val departmentLiveData: MutableLiveData<List<Department>> by lazy { MediatorLiveData<List<Department>>() }
    var candidate: Candidate = Candidate(department = Department())

    var selectedDepartment: Department? = null

    var selectedDate = DateTime()

    val candidateLiveData: MutableLiveData<CandidateWithInterview> by lazy { MediatorLiveData<CandidateWithInterview>() }

    fun insertCandidate() {
        postValue(loadingLiveData, showProgress = true) {
            insertDepartmentIfRequired().flatMap {
                candidate.department = it
                formDataRepo.insertForm(it.id)
            }.flatMap { form ->
                candidateRepo.addCandidate(candidate, Interview("", selectedDate, formId = form.id))
            }.map {
                true
            }
        }
    }


    fun fetchCandidate() {
        postValue(candidateLiveData, true) {
            candidateRepo.findCandidateById(candidateId).map {
                candidate = it.candidate
                it
            }
        }

    }

    private suspend fun insertDepartmentIfRequired(): Either<Exception, Department> {
        return selectedDepartment?.let {
            if (it.id.isBlank()) {
                departmentRepo.insertDepartment(it)
            } else {
                Either.Right(it)
            }
        } ?: Either.Left(RuntimeException("Depart is not selected"))
    }

    fun fetchDepartments() {
        postLiveValue(departmentLiveData as MediatorLiveData<List<Department>>, showProgress = false) {
            departmentRepo.getAllDepartments()
        }
    }

    fun insertInterview() {

    }
}
package com.bosleo.feedback.presentation.core

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bosleo.feedback.utility.Either
import kotlinx.coroutines.*
import org.koin.standalone.KoinComponent
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel : ViewModel(), CoroutineScope, KoinComponent {

    val exceptionLiveData: MutableLiveData<Exception> = MutableLiveData()
    val loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }


    fun <E : Exception, R> postValue(
        successLiveData: MutableLiveData<R>,
        showProgress: Boolean = true,
         response: suspend () -> Either<E, R>
    ) {
        if (showProgress) {
            loadingLiveData.postValue(true)
        }
        launch {
            response().either({
                exceptionLiveData.postValue(it)
            }, {
                successLiveData.postValue(it)
            })
            if (showProgress) {
                loadingLiveData.postValue(false)
            }
        }
    }


    fun <E : Exception, R> postLiveValue(
        successLiveData: MediatorLiveData<R>,
        showProgress: Boolean = true,
        response: suspend () -> Either<E, LiveData<R>>
    ) {
        if (showProgress) {
            loadingLiveData.postValue(true)
        }
        launch {
            response().either({
                exceptionLiveData.postValue(it)
            }, {
                //addSource method internally calls observerForever
                //which needs to be called from main thread
                launch {
                    withContext(Dispatchers.Main) {
                        successLiveData.addSource(it) {
                            successLiveData.postValue(it)
                        }
                    }
                }
            })
            if (showProgress) {
                loadingLiveData.postValue(false)
            }
        }
    }


    fun <E : Exception, R> postValue(
        successLiveData: MutableLiveData<R>,
        response: suspend () -> Either<E, R>,
        result: (Boolean) -> Unit
    ) {
        launch {
            response().either({
                result.invoke(false)
            }, {
                result.invoke(true)
                successLiveData.postValue(it)
            })
        }

    }
}
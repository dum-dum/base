package com.bosleo.feedback.presentation.form.adapter

import android.os.Parcelable
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bosleo.feedback.R
import com.bosleo.feedback.model.Form
import com.bosleo.feedback.utility.setRandomBackground
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.cell_department.view.*
import kotlinx.android.synthetic.main.flip_frontview.view.*

class FormAdapterItem(val form: Form, val departmentName: String) :
    AbstractFlexibleItem<FormAdapterItem.FormListViewHolder>() {
    var viewHolder: FormListViewHolder? = null

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FormListViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {

        holder?.itemView?.apply {
            tvDepartment.text = departmentName
            ivAvatar.setRandomBackground()
            ivAvatar.setText(departmentName.first().toString())
        }
    }

    override fun equals(other: Any?) = other is FormAdapterItem && other.form.id == form.id
    override fun onViewAttached(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FormListViewHolder?,
        position: Int
    ) {
        super.onViewAttached(adapter, holder, position)
        viewHolder = holder
    }

    override fun unbindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: FormListViewHolder?,
        position: Int
    ) {
        viewHolder = null
        super.unbindViewHolder(adapter, holder, position)
    }

    override fun createViewHolder(
        view: View,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
    ) = FormListViewHolder(view, adapter)


    override fun getLayoutRes() = R.layout.cell_department

    override fun hashCode(): Int {
        var result = form.hashCode()
        result = 31 * result + departmentName.hashCode()
        return result
    }

    inner class FormListViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>) :
        FlexibleViewHolder(view, adapter)
}
package com.bosleo.feedback.presentation.interview

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.bosleo.feedback.data.interview.contract.InterviewRepo
import com.bosleo.feedback.model.Interview
import com.bosleo.feedback.presentation.core.BaseViewModel
import com.bosleo.feedback.utility.asMediatorLiveData
import org.koin.standalone.inject

class RemarksViewModel : BaseViewModel() {

    val interviewRepo by inject<InterviewRepo>()
    var interviewId = ""
    val submitted = false

    val interviewLiveData: MutableLiveData<Interview> by lazy { MediatorLiveData<Interview>() }

    fun fetchInterview() {

        postLiveValue(interviewLiveData.asMediatorLiveData()) {
            interviewRepo.getInterviewByID(interviewId)
        }

    }

    fun updateStatus(comments: String, result: String) {
        postValue(interviewLiveData) {
            interviewRepo.updateInterview(interviewLiveData.value!!.also {
                it.comment = comments
                it.result = result
            })
        }
    }

}
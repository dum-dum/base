package com.bosleo.feedback.presentation.core

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<T, VH : BaseViewHolder<T>> : RecyclerView.Adapter<VH>() {

    private var items: List<T> = arrayListOf()

     final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        getViewHolder(LayoutInflater.from(parent.context), parent, viewType)

    final override fun getItemCount() = items.size

    final override fun onBindViewHolder(holder: VH, position: Int) = holder.bind(items[position])

    fun setData(items: List<T>) {
        this.items = items
        notifyDataSetChanged()
    }

    protected abstract fun getViewHolder(inflater: LayoutInflater, parent: ViewGroup, viewType: Int): VH

}
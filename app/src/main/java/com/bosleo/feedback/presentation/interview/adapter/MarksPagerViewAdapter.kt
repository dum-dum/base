package com.bosleo.feedback.presentation.interview.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.bosleo.feedback.model.SkillWithSubSkill
import com.bosleo.feedback.presentation.interview.FeedbackViewModel
import eu.davidea.flexibleadapter.FlexibleAdapter

class MarksPagerViewAdapter(
    private val skills: List<SkillWithSubSkill>,
    private val color: Int,
    private val feedbackViewModel: FeedbackViewModel
) : PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val recyclerView = RecyclerView(container.context)
        recyclerView.layoutManager = LinearLayoutManager(container.context)
        recyclerView.adapter = FlexibleAdapter<SkillMarkAdapterItem>(skills[position].subSkill.map {
            SkillMarkAdapterItem(it,color,feedbackViewModel)
        })

        container.addView(recyclerView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        return recyclerView
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return skills[position].skill.name
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


    fun getItemAtPosition(position: Int): SkillWithSubSkill? {
        return if (position < skills.size) {
            skills[position]
        } else {
            null
        }
    }

    override fun getCount() = skills.size
}
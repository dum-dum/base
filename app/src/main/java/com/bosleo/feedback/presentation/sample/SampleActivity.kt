package com.bosleo.feedback.presentation.sample

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import androidx.lifecycle.Observer
import android.graphics.Color
import android.os.Bundle
import androidx.annotation.NonNull
import com.google.android.material.circularreveal.CircularRevealCompat
import com.google.android.material.circularreveal.CircularRevealWidget
import androidx.core.content.ContextCompat
import android.util.Log
import android.view.View
import com.bosleo.feedback.R
import com.bosleo.feedback.data.candidate.datasource.remote.CandidateApiService
import com.bosleo.feedback.presentation.core.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_sample.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class SampleActivity : BaseActivity<SampleViewModel>() {

    private val sampleViewModel: SampleViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)
        init(savedInstanceState)
        circularRevealFromMiddle(root,addButton)
    }

    override fun getViewModel(): SampleViewModel {
        return sampleViewModel
    }


    private fun init(savedInstanceState: Bundle?) {
        attachSampleObserver()
        addButton.setOnClickListener {
            sampleViewModel.addCandidate()
        }
        fetchData()

        val sampleApiService by inject<CandidateApiService>()

        sampleApiService.login("roman@gmail.com", "Roman12345").flatMap {
            sampleApiService.getInterViewMark()
        }.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d("data", "$it")
            }, {
                it.printStackTrace()
            }).apply {

            }
    }

    private fun fetchData() {
        sampleViewModel.fetchData()

    }

    private fun attachSampleObserver() {
        sampleViewModel.candidateLiveData.observe(this, Observer {
            println(it)
        })
    }

    private fun <T> circularRevealFromMiddle(@NonNull circularRevealWidget: T,anchorView:View) where T : View, T : CircularRevealWidget {
        circularRevealWidget.post {
            val viewWidth = circularRevealWidget.width
            val viewHeight = circularRevealWidget.height

            val viewDiagonal = Math.sqrt((viewWidth * viewWidth + viewHeight * viewHeight).toDouble()).toInt()

            val animatorSet = AnimatorSet()
            animatorSet.playTogether(
                CircularRevealCompat.createCircularReveal(
                    circularRevealWidget,
                    anchorView.x +anchorView.width/2,
                    anchorView.y +anchorView.height/2,
                    10f,
                    (viewDiagonal / 2).toFloat()
                ),
                ObjectAnimator.ofArgb(
                    circularRevealWidget,
                    CircularRevealWidget.CircularRevealScrimColorProperty.CIRCULAR_REVEAL_SCRIM_COLOR,
                    ContextCompat.getColor(this, R.color.colorAccent),
                    Color.TRANSPARENT
                )
            )

            animatorSet.duration = 5000
            animatorSet.start()
        }
    }
}

package com.bosleo.feedback.presentation.interview.adapter

import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bosleo.feedback.R
import com.bosleo.feedback.model.SubSkill
import com.bosleo.feedback.presentation.interview.FeedbackViewModel
import com.bosleo.feedback.utility.gone
import com.bosleo.feedback.utility.visible
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import kotlinx.android.synthetic.main.cell_marks.view.*

class SkillMarkAdapterItem(
    private val subSkill: SubSkill,
    private val darkColor: Int,
    private val feedbackViewModel: FeedbackViewModel
) :
    AbstractFlexibleItem<SkillMarkAdapterItem.SubSkillListViewHolder>() {

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>?,
        holder: SubSkillListViewHolder?,
        position: Int,
        payloads: MutableList<Any>?
    ) {
        val max = 10
        val min = 0
        val total = max - min

        holder?.itemView?.sMarks?.positionListener =
                { pos ->
                    holder?.itemView?.sMarks?.bubbleText = "${min + (total * pos).toInt()}"
                    feedbackViewModel.updateMarkToViewModel(subSkill,min + (total * pos).toInt())
                }
        holder?.itemView?.sMarks?.startText = "$min"
        holder?.itemView?.sMarks?.endText = "$max"
        holder?.itemView?.sMarks?.colorBar = darkColor
        holder?.itemView?.sMarks?.colorBubble = Color.WHITE

        holder?.itemView?.sMarks?.beginTrackingListener = {
            holder?.itemView?.tvSkill?.gone()
        }

        holder?.itemView?.sMarks?.endTrackingListener = {
            holder?.itemView?.tvSkill?.visible()
        }

        feedbackViewModel.marks.find {
            it.subSkillId == subSkill.id
        }?.apply {
            holder?.itemView?.sMarks?.position = mark/10f
        }



        holder?.itemView?.apply {
            tvSkill.text = subSkill.name.capitalize()
        }
    }



    override fun equals(other: Any?) = other is SkillMarkAdapterItem && other.subSkill.id == subSkill.id

    override fun createViewHolder(
        view: View,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
    ) = SubSkillListViewHolder(view, adapter)


    override fun getLayoutRes() = R.layout.cell_marks
    override fun hashCode(): Int {
        return subSkill.hashCode()
    }


    inner class SubSkillListViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>) :
        FlexibleViewHolder(view, adapter)
}
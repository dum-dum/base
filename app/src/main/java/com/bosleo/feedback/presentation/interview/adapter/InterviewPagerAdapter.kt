package com.bosleo.feedback.presentation.interview.adapter

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.bosleo.feedback.presentation.interview.fragment.TodayInterviewFragment
import com.bosleo.feedback.presentation.interview.fragment.UpcomingInterviewFragment

class InterviewPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    private val fragments = arrayListOf(
        TodayInterviewFragment(),
        UpcomingInterviewFragment()
    )

    override fun getItem(position: Int) = fragments[position]

    override fun getCount() = fragments.size

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Today"
            1 -> "Upcoming"
            else -> ""
        }
    }

}
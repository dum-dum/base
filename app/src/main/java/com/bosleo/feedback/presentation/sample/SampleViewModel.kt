package com.bosleo.feedback.presentation.sample

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.bosleo.feedback.data.candidate.contract.CandidateRepo
import com.bosleo.feedback.model.Candidate
import com.bosleo.feedback.presentation.core.BaseViewModel
import kotlinx.coroutines.launch

class SampleViewModel constructor(private val candidateRepo: CandidateRepo) : BaseViewModel() {

    val candidateLiveData: MutableLiveData<List<Candidate>> by lazy { MediatorLiveData<List<Candidate>>() }

    fun fetchData() {
//        postLiveValue(candidateLiveData as MediatorLiveData<List<Candidate>>) {
//            candidateRepo.getAllCandidate().map {
//                Transformations.map(it) { candidatesLiveData ->
//                    candidatesLiveData.map { candidate ->
//                        with(candidate) {
//                            Candidate(id, name, departmentId, company, date, name)
//                        }
//                    }
//                }
//            }
//        }
    }

    fun addCandidate() {

    }
}
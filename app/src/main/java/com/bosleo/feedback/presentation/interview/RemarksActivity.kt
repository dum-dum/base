package com.bosleo.feedback.presentation.interview

import android.graphics.Color
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import com.bosleo.feedback.R
import com.bosleo.feedback.presentation.core.BaseActivity
import com.bosleo.feedback.utility.Constants.EXTRA_CIRCULAR_REVEAL_COLOR
import com.bosleo.feedback.utility.Constants.EXTRA_INTERVIEW_ID
import kotlinx.android.synthetic.main.activity_remarks.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RemarksActivity : BaseActivity<RemarksViewModel>() {

    val remarksViewModel: RemarksViewModel by viewModel()
    var revealColor = Color.BLACK

    override fun getViewModel(): RemarksViewModel = remarksViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_remarks)
        init()
        attachObservable()
    }

    fun init() {
        remarksViewModel.interviewId = intent?.getStringExtra(
            EXTRA_INTERVIEW_ID
        ).orEmpty()
        revealColor = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_COLOR, revealColor)

        remarksViewModel.fetchInterview()

        appbar.setBackgroundColor(revealColor)
        toolbar.setBackgroundColor(revealColor)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        spResult.adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_list_item_1,
            android.R.id.text1,
            resources.getStringArray(R.array.Result)
        )

        fabDone.setOnClickListener {
            if (spResult.selectedItemPosition < 1) {
                spResult.error = "Please select any result"
            } else {
                remarksViewModel.updateStatus(etComments.text.toString(), spResult.selectedItem as String)
            }
        }
    }

    fun attachObservable() {
        remarksViewModel.interviewLiveData.observe(this, Observer {
            it?.apply {
                etComments.setText(comment)
                etComments.setSelection(comment.length)
                when (result.toLowerCase()) {
                    "pass" -> spResult.setSelection(1)
                    "fail" -> spResult.setSelection(2)
                    "pending" -> spResult.setSelection(3)
                }

            }
        })
    }
}

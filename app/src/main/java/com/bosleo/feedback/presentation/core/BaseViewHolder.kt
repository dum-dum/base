package com.bosleo.feedback.presentation.core

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

abstract class BaseViewHolder<T>(itemView: View, override val containerView: View? = itemView) :
    RecyclerView.ViewHolder(itemView), LayoutContainer {

    abstract fun bind(data: T)

}
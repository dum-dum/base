package com.bosleo.feedback.presentation.core

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import android.view.View
import com.bosleo.feedback.R

abstract class BaseFragment : androidx.fragment.app.Fragment() {

    var rootView: View? = null

    fun addFragment(
        @IdRes containerId: Int, fragment: androidx.fragment.app.Fragment,
        addToBackStack: Boolean = true,
        fadeIn: Boolean = false
    ) {
        with(childFragmentManager.beginTransaction()) {
            if (addToBackStack) {
                addToBackStack(fragment.javaClass.simpleName)
            }
            if (!fadeIn) {
                setCustomAnimations(R.anim.slide_in, R.anim.slide_out, R.anim.slide_in, R.anim.slide_out)
            } else {
                setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
            }
            add(containerId, fragment)
            commit()
        }
    }

    fun replaceFragment(@IdRes containerId: Int, fragment: androidx.fragment.app.Fragment, fadeIn: Boolean = false) {
        with(childFragmentManager.beginTransaction()) {
            if (!fadeIn) {
                setCustomAnimations(R.anim.slide_in, R.anim.slide_out, R.anim.slide_in, R.anim.slide_out)
            } else {
                setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out)
            }
            replace(containerId, fragment)
            addToBackStack(null)
            commit()
        }
    }

    fun attachFragment(fragment: androidx.fragment.app.Fragment) {
        with(childFragmentManager.beginTransaction()) {
            attach(fragment)
            commit()
        }
    }

    fun detachFragment(fragment: androidx.fragment.app.Fragment) {
        with(childFragmentManager.beginTransaction()) {
            detach(fragment)
            commit()
        }
    }

    fun removeFragment(fragment: androidx.fragment.app.Fragment) {
        with(childFragmentManager.beginTransaction()) {
            remove(fragment)
            commit()
        }
    }

}